layout: true
class: typo, typo-selection
site-title: Generative Design - Cours - B1

---

count: false
class: left, middle, nord-light

# **generative & interactive design**

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light
<br/><br/>

Module 1: [Courses](module-1.html) / [Exercises](module-1.ex.html)

Module 2: [Courses](module-2.html)

------

[Reference](reference.html) / [Practice](practice.html)
