layout: true
class: typo, typo-selection
site-title: Generative Design - Module 2 - Cours - B1

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 2: flow control, maths and interaction

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>



---

class: nord-dark

# Flow-control: for, if, conditions

---

class: nord-light

## Drawing a lot of lines

```js
function setup() {
  let size = 100;
  let separation = 10;

  line(0, 0 * separation, size, 0 * separation);
  line(0, 1 * separation, size, 1 * separation);
  line(0, 2 * separation, size, 2 * separation);
  line(0, 3 * separation, size, 3 * separation);
  line(0, 4 * separation, size, 4 * separation);
  line(0, 5 * separation, size, 5 * separation);
}
```

---

class: nord-light

## Drawing a lot of lines

```js
function setup() {
  let size = 100;
  let separation = 10;

  // Horizontal
  for(let i = 0; i < 5; i ++) {
    line(0,  i * separation, size, i * separation);
  }

  // Vertical
  for(let i = 0; i < 5; i ++) {
    line(i * separation, 0, i * separation, size);
  }
}
```

---

class: nord-light

## Drawing a lot of lines

```js
function setup() {
  let size = 100;
  let separation = 10;
  // Both combined: less code, less opportunity for bugs
  // Best code is the one you don't write !
  for(let i = 0; i < 5; i ++) {
    line(0, i * separation,size, i * separation);
    line(i * separation, 0, i * separation, size);
  }
}
```

---

class: nord-light

## Exercice: drawing a grid

Reproduce the following (The function `square` must be written only once in the code !)

![grid](assets/grid.png)

---

class: nord-light

## What if we want the even lines to be in red ?

```js
function setup() {
  let size = 100;
  let separation = 10;
  for(let i = 0; i < 5; i ++) {
    // ????

    line(0, i * separation,size, i * separation);
    line(i * separation, 0, i * separation, size);
  }
}
```

---

class: nord-light

## What if we want the even lines to be in red ?

```js
function setup() {
  let size = 100;
  let separation = 10;
  for(let i = 0; i < 5; i ++) {

    /* if i is even */ stroke(255, 0, 0);
    /* else, if i is odd */ stroke(0, 0, 0);

    line(0, i * separation,size, i * separation);
    line(i * separation, 0, i * separation, size);
  }
}
```

---

class: nord-light

## What if we want the even lines to be in red ?

```js
function isEven(x)
{ return /* some computation on x*/; }

function setup() {
  let size = 100;
  let separation = 10;
  for(let i = 0; i < 5; i ++) {

    if(isEven(i)) {
      stroke(255, 0, 0);
    } else {
      stroke(0, 0, 0);
    }

    line(0, i * separation,size, i * separation);
    line(i * separation, 0, i * separation, size);
  }
}
```

---

class: nord-light

# Understanding if

```js
if ( /* condition */ ) {
  // this code executes if the condition is true
}
```

```js
if ( /* condition */ ) {
  // this code executes if the condition is true
} else {
  // this code executes if the condition is false
}
```

---

class: nord-light

# Understanding if

```js
if ( /* first condition */ ) {
  // this code executes only if the condition is true
} else if( /* second condition */) {
  // this code executes only if the first condition is not true
  // and the second is true
} else if( /* third condition */) {
  // this code executes only if the first and second conditions are not true
  // and the third is true
} else {
  // all remaining cases (not mandatory !)
}
```

---

class: nord-light

# Understanding conditions

Conditions are *expressions* that *yield* a *boolean value*.
* boolean: something that can be true or false.
* JS examples (one per line):

```js
true
false
1
0
1 === 0
1 === 1
1 > 0
1 <= 0
1 + 1 === 2
1 + 1 === 6 - 4
isEven(2)
```

---

class: nord-light

# Combining conditions

How to say, "x is greater than 10 and smaller than 100" ?
```js
if(x > 10)
{
  if(x < 100)
  {
    // both conditions are true
  }
}
```

**BUT THIS IS NOT THE WAY WE WILL DO IT !**

---

class: nord-light

# Boolean operations: and (&&), or (||), not (!)

Like + and *, but for booleans:

```js
(x > 0) && (x < 100)
(x >= 0) && (x <= 100)
(x < 0) || (x > 10)
(x > 10) && isEven(x)
(x > 10) && (!isEven(x))
(x === 3) || (isEven(x))
(x === random(0, 10))
```


---

class: nord-dark

# Exercise

- Draw circles at random positions, but only if they verify the following:
`sin(x/10) * cos(y/10) > 0.45`

- Play with colors, number of circles, blend modes... (e.g. `blendMode(SCREEN)`).

---

class: nord-dark

# Free composition !

You've got 10 minutes :-)

---
class: nord-dark, center, middle

# Interaction

---
class: nord-light

# Using the mouse position

`mouseX`, `mouseY` give the current position of the mouse cursor.

```js
function setup() { }
function draw() {
  background(220, 220, 220, 20);
  line(0, 0, mouseX, mouseY);
}
```

---
class: nord-light

# Using the mouse position (cont.)

From the center: note that the mouse coordinates *aren't* affected by `translate` and thus must be corrected.

```js
function setup() { }
function draw() {
  background(220, 220, 220, 20);
  translate(width/2, height/2);
  line(0, 0, mouseX - width/2, mouseY - height/2);
}
```

---
class: nord-light

# Using the mouse press (option A)

```js
function setup() { }
function draw() {
  background(220, 220, 220, 20);
  translate(width/2, height/2);

  if(mouseIsPressed) {
    stroke(255, 0, 0);
  } else {
    stroke(0, 0, 0);
  }

  line(0, 0, mouseX - width/2, mouseY - height/2);
}
```
---
class: nord-light

# Using the mouse press (option B)

```js
function setup() { }
function draw() { }

// Called by p5 automatically when the mouse is pressed
function mousePressed() {
  ellipse(mouseX, mouseY, 100, 100);
}
```
---
class: nord-dark

# Free composition !

You've got 10 minutes :-)

---
class: nord-light

# Using the text function

```js
function setup() { }
function draw() {
  background(200, 200, 200, 40);
  let x = random(0, width);
  let y = random(0, height);
  rotate(random());
  // text must be put between single or double quotes
  text("hello world", x, y);
}
```

---
class: nord-light

# Using the keyboard

```js
function setup() { }
function draw() {
  background(200, 200, 200, 40);
}

// Called by p5 automatically when a key is pressed
function keyTyped() {
  let x = random(0, width);
  let y = random(0, height);
  text(key, x, y);
}
```

---
class: nord-dark

# See all the other input methods available in the reference

Section "Events".

p5 can work on mobile phone and use your mobile phone rotation, inclination, etc.
Try to make something with it !



---

class: nord-dark, middle

# Cheat sheet: function syntax
```js
function myFunction(var a, var b) {
  line(a + b, a - b, 100, 100);
}
...

myFunction(10, 50);
```

---

class: nord-dark, middle

# Cheat sheet: variables and if/else
```js
// Comment
/* Also comment */

// Variable declaration
let myVariable = /* some value */;

// Assignment
myVariable = /* some other value */;

// Conditions
if(myCondition) {

} else if(myOtherConditon) {

} else {

}
```

---

class: nord-dark, middle

# Cheat sheet (for)
```js
// Loops
for(let myCounter = 0; myCounter < max; myCounter++) {
  // this is repeated "max" times
}

for(/* counter initialization*/;
    /* loop condition */;
    /* post-loop operation */) {
}

```


---

class: nord-dark

# Introduction to trigonometry

---

class: nord-dark, center, middle

# Exercice: let's make a triangle

---

class: nord-dark, center, middle

# Exercice: let's make a square

---

class: nord-dark, center, middle

# Exercice: let's make a pentagon

---

class: nord-light

# Super super important formulas

Trigonometry is used all over the place in generative design.

Go from polar coordinates to cartesian coordinates.
<p>$$\Large x = r * cos(\theta)~~~~~y = r * sin(\theta)$$</p>

Go from cartesian coordinates to polar coordinates.
<p>$$\Large r = sqrt(x*x + y*y)~~~~~\theta = atan2(y, x)$$</p>

(things are a bit more complicated but we'll skip that for now :-))

See https://editor.p5js.org/jcelerier/sketches/acPp467og for an animation.

---
class: nord-dark

# Parametric equations

Parametric equations: what we've been doing when setting `x=sin(...), y=cos(...)`.

General idea: define a curve with
```js
let x = a_function_of(t);
let y = another_function_of(t);
// 3D : let z = yet_another_function_of(t);
```