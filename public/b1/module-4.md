layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 4: group project

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>


---
class: easy-dark, center

# Installing vs.code for remote collaboration

---
class: nord-light

# How it will work
* Step 1: project definition / selection
* Step 2: case study
* Step 3: realisation

---
class: nord-light

# What I expect
* The code
* A small doc (1 page) that explains how you did it
* There will be  a 5 minute presentation

---
class: nord-dark, center

# Simple  projects

---
class: nord-light

# Simple project 1: fun with images

It is possible to load and edit images in p5.js with `loadImage` and `loadPixels`.

Make a program that destroys an image.

**Important** It will be very slow if you use large images. Try to not use more than 500x500 images to keep it manageable.

---
class: nord-light

# Simple project 2:

![](assets/example.png)

---
class: nord-light

# Simple project 3: fun with text

- Learn how to use the p5.js `loadFont` and `textToPoints` functions.
- Make something interactive with it.

---
class: nord-light

# Simple project 4: generative music

- Hint: https://p5js.org/reference/#/libraries/p5.sound
- Guideline:
![](assets/card.jpeg)


---
class: nord-light

# Simple project 5: dataset

- It is possible to read data files with P5, with `loadJson`.
- Create a pretty animated display of some geolocalized data:

  * https://data.cityofnewyork.us/resource/9hxz-c2kj.json
  * https://data.cityofnewyork.us/resource/i4gi-tjb9.json
  * https://data.cityofnewyork.us/resource/h9gi-nx95.json
  * https://data.cityofnewyork.us/Transportation/Subway-Entrances/drex-xx56

Other datasets: https://data.cityofnewyork.us/browse?sortBy=most_accessed&utf8=%E2%9C%93

---
class: nord-dark, center

# Hard projects

---
class: nord-light

# Hard project 1: fish pond

* https://www.youtube.com/watch?v=KLTMGFWpkro
* https://www.thisiscolossal.com/2016/08/interactive-koi-teamlab/
* https://www.youtube.com/watch?v=969eAMj05JY

---
class: nord-light

# Hard project 2: interactive map

* A websites that shows countries individually - the user can choose which country is displayed.
* Countries morph into each other.
* Must look fancy.
* Ideally works on mobile.

---
class: nord-light

# Hard project 3: particle sketch

* Given a picture, use particles to sketch its main features / outline / ... in an animated way.

* Possible inspiration: https://www.youtube.com/watch?v=JkcmwCotVew


---
class: nord-light

# Hard project 4: procedural tattoos

* Generate tattoo patterns procedurally.
* Display them on a 3d model that can be rotated.

* Possible inspiration:

  https://www.superhitideas.com/amazing-abstract-tattoo-designs-collection/


---
class: nord-light

# Hard project 5: Garden of the Earthly Delights

* Provide your own interpretation of the famous Hieronymus Bosch painting.
* Has to be animated in a meaningful way.

![the painting](https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/The_Garden_of_earthly_delights.jpg/1280px-The_Garden_of_earthly_delights.jpg)

---
class: nord-light

# Hard project 6: a window on the world

* The material: https://www.insecam.org/en/byrating/
* Possible inspirations:
 - https://rndr.studio/projects/hyperlocator/
 - https://www.stanza.co.uk/
 - https://www.theartstory.org/movement/video-art/
 - https://www.pinterest.fr/pin/449374869047291462/

---
class: nord-light

# Hard project 7: old-school fun

* Something that reminds old video games and computers
* Inspiration: https://vimeo.com/462343366
* Inspiration: https://pouet.net
  * For instance, making a few dozen seconds of animations and transitions that look like the ones here: https://www.amigafrance.com/revision-2020-le-best-of/
  or here: https://www.youtube.com/watch?v=zwoyfH7TgEQ



