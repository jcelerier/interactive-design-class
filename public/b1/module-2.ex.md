layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 2.1: exercices

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>



---
class: nord-light

# Exercice 10 - drawing

Make something that draws when the mouse is pressed on the canvas, like the pen tool in paint software.

What can we use to draw ?


---
class: nord-light

# Exercice 11 - more fun with drawing

- Extend the drawing tool, so that it will draw symmetrically.
- Extend the drawing tool, so that the pen weight depends on the speed.

Hints: `movedX`, `movedY`, Pythagor's theorem

---
class: nord-light

# Exercice 12 - drawing parametric equations

```js
y = f(x)
```

* Draw this for the function `y = 100 * sin(x / 10)` in the middle of the screen.
* Also try with `y = 100 * sin((x - mouseX) / 10)`
* Experiment with the various math functions in https://p5js.org/reference/
* Use mouseX, mouseY for various parameters
* Hints: `translate`

---
class: nord-light

# Exercice 13 - freeform

* Hints: play with `rotate(0.0001 * mouseY)`, `blendMode(SCREEN)`, not redrawing the background, drawing with ellipses, lines, squares...

---
class: nord-light

# Exercice 14

Make a program that generates artworks in the following style:

  ![circle](../static/circle-small.png)
  ![circle](../static/circle2-small.png)
  ![circle](../static/circle3-small.png)

The position of a point on a circle is given by:
```js
let x = r * cos(theta); // r is the circle radius.
let y = r * sin(theta); // theta is the angle.
```
Strongly inspired by https://www.instagram.com/entropismes


---
class: nord-light

# Exercice 15 - mouse area

* Draw a grid of circles
* Make the radius of the circles depend on the distance to the mouse cursor.
* Hints: `dist`


---
class: nord-light

# Exercice 16 - mouse area

* Extend the previous exercise: if the shapes are within the distance, draw them as circles, otherwise, draw them as squares.



---
class: nord-light

# Exercice 17 - field repulsion

* Instead of drawing circles and squares, draw lines. The lines should be repulsed by the mouse pointer


---
class: nord-light

# Exercice 18 - grid patterns

For the 31/03/2020 23:59 Europe/Paris timezone:


Inspired by sketches such as:
- https://openprocessing.org/sketch/219299
- https://openprocessing.org/sketch/1126676
- https://openprocessing.org/sketch/977026
- https://openprocessing.org/sketch/735051
- https://twitter.com/jcelerie/status/1346137939475124225/photo/1

Make something interactive in a grid.