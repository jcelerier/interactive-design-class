layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 1.2: homework correction

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light

# Exercice 9

https://editor.p5js.org/jcelerier/sketches/u0Ig5MMVG
