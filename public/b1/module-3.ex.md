layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 3.1: exercices

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>



---
class: nord-light

# Exercice 19 - shanpes

![shanpes](assets/shanpes.png)

---
class: nord-light

# Exercice 19.1 - shanpes

Draw a triangle

---
class: nord-light

# Exercice 19.2 - shanpes

Draw a square

---
class: nord-light

# Exercice 19.3 - shanpes

Draw a pentagon

..?


---
class: nord-light

# Exercice 19 - howto ?

https://p5js.org/reference/#/p5/beginShape

Note the formulas:
Given a radius `r` and an angle `theta`, one draws the point at polar coordinates `(r, theta)` with:
```js
x = r * cos(theta);
y = r * sin(theta);
```

---
class: nord-light

# Exercice 20 - playing with shapes

1. Draw a custom shape of your liking with `beginShape`, `vertex`, `endShape`.

---
class: nord-light

# Exercice 21 - encapsulating shapes

1. Put the shape drawing code in a function.

Reminder: the syntax is:

```js
function nameOfMyFunction(... arguments goes here ...) {
  // code goes here
}
```
e.g.

```js
function smallCircle(x, y, size) {
  circle(x, y, size / 10);
}
```

---
class: nord-light

# Exercice 21 - encapsulating shapes

2. Allow the user to draw your shape at any position.

To do that, use `push`, `pop`, `translate`.

---
class: nord-light

# Exercice 22 - animating shapes

1. Add some animation to your shape's shape. For instance, by using `noise`.

2. Draw a lot of random shapes at random positions on the screen.

3. At any given point in time, make your shapes move towards the mouse pointer.


---
class: nord-light

# Exercice 23 - collisions

- Make a program that creates a circle that falls towards the bottom of the screen.
- It has to bounce back at the bottom of the screen.

---
class: nord-light

# Exercice 24 - animated shapes

- Draw the sin function.
- Animate it by using the frame count in the phase.
- Draw a lot of those at different speeds and shapes.

Note: put them in an array !
