layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 1.1: exercices

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light

# Quizz 1

* Is this valid code ?

1. `ellipse(100, 100, 50, 50);`
2. `let taille = 2;`
3. `let ellipse = 2;`
4. `function setup() rect(0, 0, 100, 100);`

---
class: nord-light

# Quizz 2

* Is this valid code ?

5. `let taille = ellipse(100, 100, 50, 50);`
6. `ellipse(let taille = 100, taille, 50, 50);`
7. `ellipse(taille = 100, taille, 50, 50);`
8. `ellipse(sin(100), 100, 50, 50);`
9. `fill(35, 10, 25, 50, 42, 17);`

---
class: nord-light

# Quizz 3

* What happens ?

10.
```js
function setup() {
  line(0, 0, 50, 50);
}
```

11.
```js
function setup() {
  strokeWeight(10);
  line(0, 0, 50, 50);
  stroke(255, 0, 0);
}
```

---
class: nord-light

# Quizz 4

* What happens ?

12.
```js
function setup() {
  let x = 10;
  line(0, 0, x + 10, x * 2 + 10);
}
```

13.
```js
function setup() {
  line(width / 2, 0, width / 2, height);
  line(0, height / 2, width, height / 2);
}
```
---
class: nord-light

# Before starting

- Install Visual Studio Code (or update it to the latest version in the Help menu)
- Install p5.vscode extension
- Create a new project: F1 (or ⌘+Shift+P on Mac) and type "Create p5.js Project"
- The code will be in the `sketch.js` file
- Press "Go Live" at the bottom right of the VSCode window to open a browser which will show your work.

---
class: nord-light

# Exercice 1: recreating an artwork
1. Reproduce something similar to
https://twitter.com/xiiixiii/status/1318868653488656384/photo/3
2. Save it, we will come back to it later !

Cheat sheet: `ellipse`, `rect`, `createCanvas`, `background`.

---
class: nord-light

# Exercice 2: drawing a sun

1. Draw a sun (a yellow circle with rays - only make a single vertical and horizontal ray).
2. Make it so that the sun is always centered, no matter the size of the canvas.
3. Make it so that the ray lengths can change with a single line of code.
4. Save it, we will come back to it later !

Cheat sheet: `circle`, `line`, `stroke`, `fill`, `translate`, `width`, `height`.

---
class: nord-light

# Exercice 3: drawing parallels
- Open a new sketch.
- Draw 5 horizontal or vertical lines of the same length next to each other, spaced by 7 pixels each.

Cheat sheet: `line`, `for(let i = 0; i < 10; i++) { ... operations that use i ... }`.

---
class: nord-light

# Exercice 4: back to the sun

* Make the number of rays of the sun a variable. The rays must be evenly spaced.
* Hint: The total angle of a circle is `2 * PI` (or `TAU`).

Cheat sheet: `for`, `rotate`, `PI` (or `TAU`).


---
class: nord-dark

# Functions

```JS
function theNameYouWant(argument1, argument2, [etc etc...]) {
  // the code of the function
}
```

```JS
let myVariable = 123;
function myFunction(argument1) {
  console.log(myVariable); // prints 123
}
function myOtherFunction(myVariable) {
  console.log(myVariable); // prints what's in the argument
}
```

---
class: nord-light

# Exercice 6: many suns

1. Put the drawing of the sun in a function.
   * Pass the position, the color and the ray length as arguments.
2. Draw a lot of suns :-)

Cheat sheet: `push`, `pop` for keeping the translation inside the function.
---
class: nord-dark

# Introducing randomness

* `random()`: returns a random number between 0 and 1.
* `random(10, 20)`: returns a random number between 10 and 20.

---
class: nord-light

# Exercice 7: random suns

* Add a bit of randomness to the length of the sun rays
* Add a bit of randomness to the size of the suns.

---
class: nord-dark

# Introducing color spaces

* `colorMode(HSB, 1, 1, 1);`: from now on, fill / stroke / background all take
   colors in Hue, Saturation, Brightness coordinates instead of Red, Green, Blue coordinates.
* See http://colorizer.org/

---
class: nord-light
# Exercice 8: more random suns

* Change the sun program to use the HSB colorspace.
* Add a bit of randomness to the brightness of the sun.

---
class: nord-light

# Exercice ???
10 minutes: free creation, it must not look like anything we've been doing today :-)

---
class: nord-light

# Exercice 9 (generalizing exercice 3)

* Make a program that generates random artworks in the same style than exercice 3.
* Use functions for the various groups of shapes !
