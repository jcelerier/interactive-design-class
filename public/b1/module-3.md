layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 3: animation

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>


---

class: nord-dark, center, middle

# Quizz !

---
class: nord-light

### Is this correct code ?

1. `let mouseX = 2;`
2. `for(let i = 0; i < 10; i++); { ellipse(0, 0, 10 - i); }`
3. `if (x = 0) { fill("red"); }`
4. `ellipse(mouseX * 2, mouseY / mouseX, mousex + mouseY);`

---
class: nord-light

### How to do the following ?

1. Draw an ellipse under the mouse cursor.
2. Draw a rectangle whenever the mouse is clicked, but not moved.

---

class: nord-dark, center, middle

# Animation

---
class: nord-light

# Animation in p5.js

No animation:

```js
function setup() {
  // Drawn once
  line(0, 0, 100, 100);
}
```

Animation:
```js
function setup() {
}
function draw() {
  // Drawn every 16 milliseconds.
  line(0, 0, 100, 100);
}
```

---
class: nord-light

# Animation in p5.js (cont.)

```js
function setup() {
}
function draw() {
  // random() will return a new value every time.
  line(0, 0, random(0, 100), 100);
}
```

Processing does not erase the canvas between successive calls to draw.

---
class: nord-light

# Animation in p5.js (cont.)

Solution: erasing the canvas.

Note: some artworks sometimes require *not* erasing the previously drawn frame.

Ex. https://www.instagram.com/p/CHQM1szCyUn/
```js
function setup() {
}
function draw() {
  background(220);
  line(0, 0, random(0, 100), 100);
}
```

---
class: nord-light

# Animation in p5.js (cont.)

Solution: erasing the canvas (partially).

Alpha-channel (transparency) allows to make previous drawings slowly fade out.

```js
function setup() {
}
function draw() {
  background(220, 220, 220, 50); // < "50" is the alpha value
  line(0, 0, random(0, 100), 100);
}
```

---
class: nord-light

# Animation in p5.js (cont.)

It's too fast.

```js
function setup() {
  frameRate(10); // call draw() 10 times per second
}
function draw() {
  background(220);
  // Now only drawn every 100 milliseconds.
  line(0, 0, random(0, 100), 100);
}
```

---
class: nord-dark

# Free composition !

You've got 10 minutes :-)

Note: use lines ! rects ! circles ! ellipses ! randomness ! HSB colors ! everything we've seen so far !

---
class: nord-light

# Controlled animation
`frameCount` is the number of frames since the animation started.

```js
function setup() { }
function draw() {
  background(220, 220, 220, 200);
  line(0, 0, frameCount, 100);
}
```

---
class: nord-light

# Cyclic animation
If we want smooth animations, a simple and efficient way is to use `sin` / `cos`.

```js
function setup() { }
function draw() {
  background(220, 220, 220, 200);
  // cos is between -1 and 1, we want it between 0, 1
  let percentage = 0.5 + 0.5 * cos(frameCount);
  line(0, 0, 100 * percentage, 100);
}
```
Too fast !

---
class: nord-light

# Cyclic animation

```js
function setup() { frameRate(5); }
function draw() {
  background(220, 220, 220, 200);
  let percentage = 0.5 + 0.5 * cos(frameCount);
  line(0, 0, 100 * percentage, 100);
}
```
Too janky !

---
class: nord-light

# Cyclic animation

```js
function setup() { }
function draw() {
  background(220, 220, 220, 200);
  let percentage = 0.5 + 0.5 * cos(0.01 * frameCount);
  line(0, 0, 100 * percentage, 100);
}
```
purrfect !

---
class: nord-light

# Cyclic animation - alternative with modulo (%)

Using modulo will make evenly-spaced animation frames, which will loop back at zero every time.
```js
function setup() { }
function draw() {
  background(220, 220, 220, 200);
  line(0, 0, frameCount % 100, 100);
}
```

---
class: nord-light

# Cyclic animation - pretty example

```js
function setup() { }
function draw() {
  background(220, 220, 220, 20);
  line(0, frameCount % 100, frameCount % 100, 100);
}
```

Exercice: make it loop back to the top in a cyclic motion.

Hint: `if` - draw the domain on paper.

---
class: nord-light

# Exercise: spinner wheel

Make something similar to the various "loading" animations.

Hint : the coordinates of the circles should also be a circle.

Cheat sheet: `translate`, `rotate`.

Note that translation / rotation *are* reset across calls to `draw`, no need to call `push`, `pop`.

---
class: nord-dark, center, middle

# Animation techniques

---
class: nord-light

# noise instead of random

Random is... random. It's every time an entirely unrelated value.
Not very smooth.

Noise:
* Like random, but smooth.
* Always between [0; 1]
* Needs an input parameter.

```js
function draw() {
  background(220, 220, 220, 20);
  let param = 100 * noise(frameCount * 0.01);
  line(0, param, param, 100);
}
```


---
class: nord-light

# From painting to objects

*** These are the most important slides in the whole course ***

So far we have been dealing with the *painter* metaphor:
- We tell a virtual painter to choose a color, to draw circles, etc.
- He does it on top of the canvas every time.

This is useful for a great deal of artworks.

Now imagine that we want to animate circles moving across the screen.
- We will be more interested in defining the motion of those circles, than by how we draw them.
- We have to "go up an abstraction layer" and define entities.
- This is how *all* video games (and the majority of "interactive experiences" are made.

---
class: nord-light

# From painting to objects

Objects will have to live "outside" of the `setup` and `draw` functions, as they have to persist between frames.
We call that "global scope" in programming languages.

```js
let x = 0;
let y = 0;
function draw() {
  background(0);
  x++;
  y++;
  ellipse(x, y, 10);
}
```

---
class: nord-dark, center, middle

# Exercise: understanding scope and variables

---
class: nord-light

# Physics

Objects in the real world react according to physics laws.
A good approximation is the [...] laws.

```js
function draw() {
  /* Compute the speed of the object*/
  ...
  /* Move the object by adding the speed to the position */
  ...
  /* Draw the object */
  ...
}
```

---
class: nord-light

# Exercise: understanding speed

- We will make a ball that bounces against the walls of the screen.
- Hint: we will need 4 global variables: `pos_x, pos_y, speed_x, speed_y`.


---
class: nord-dark

# Free composition !

You've got until the end of the class :-)