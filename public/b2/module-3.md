layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 3: advanced techniques

Jean-Michaël Celerier 

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>


---

class: nord-dark, center, middle

# An overview of useful advanced techniques

---

class: nord-light

# Reactive design

We can tell processing to resize itself whenever the user resizes the window.

```js
function setup() {
  createCanvas(windowWidth, windowHeight);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
```

Remember: use `width`, `height` in your work to use the size of the current canvas. 

---
class: nord-light

# Custom canvases
```js
// Creating the canvas
let canvas = createGraphics(100, 100);

// Drawing on it: same functions that we used previously can be used 
// as methods 
canvas.stroke(120);
canvas.circle(0, 0, 100);

// Render the canvas to our screen (in setup() / draw())
image(canvas, 0, 0);
```

---
class: nord-light

# Blend modes

Just like the modes in graphics software: overlay, screen, add, multiply, etc...
Define how semi-transparent pixels are blended together when you draw on top.

List of all the available blend modes: https://p5js.org/reference/#/p5/blendMode
```js
blendMode(MULTIPLY);
strokeWeight(30);
stroke(80, 150, 255);
line(25, 25, 75, 75);
stroke(255, 50, 50);
line(75, 25, 25, 75);
```

---
class: nord-light

# Advanced shapes

This draws a closed arbitrary shape according to the positions given.
```js
beginShape();
vertex(0, 0);
vertex(1, 1);
vertex(2, 7);
vertex(0, 0);
endShape(CLOSE); 
```

Exercice: draw a circle by using this function.

---
class: nord-light

# Advanced shapes

Exercise: 
1. Create a shape by storing the successive mouse positions 
in an array when the mouse is pressed. Draw the in-progress shape with a stroke.
When the mouse is released, fill the shape.

2. Extend this code to support multiple shapes.

3. Instead of just storing positions, create a Shape class with an associated fill color.

---

class: nord-light

# Math functions that are often useful

```js
// Linear interpolation: go from a number to another, smoothly
lerp()

// Map a number from a domain (e.g. [-1; 1]) to another (e.g. [0; 2* PI]))
map()

// Compute the distance between two points
dist()

// Make sure that a number never goes outside of fixed bounds
constrain()

// Round a floating-point number.
round()
```

---

class: nord-dark, center, middle

# Morphogenesis

---
class: nord-light

# A free book to read

https://natureofcode.com/

This chapter of the course leverages a lot of the content and ideas of that book.

---
class: nord-light

# Natural-looking randomness: Perlin noise

A random function where given "close" successive inputs, the outputs are smooth
between 0 and 1. 
```js
noise(0)
noise(0.01)
noise(0.02)...

// Try this function with smoothness equal to 1, 0.1, 0.01, etc..
function draw_noise(smoothness) {
  for(let i = 0; i < width; i++) {
    stroke(noise(i * smoothness) * 255);
    line(i, 0, i, height);
  }
}
```

Exercice: draw a circle with some noise added for the coordinates. 
Increase the amount of points to make flower-like things.

---
class: nord-light

# Mechanics

How do we create attraction ?

* We want to simulate physics.
* Items have an acceleration, a speed, and a position.
* With a mass and the laws of gravity, a lot of realistic physical behaviour can be implemented.
* Various additional laws can be implemented to make more realistic behaviours: 
  - Friction
  - Wind resistance

---
class: nord-light

# Herboristery

How do we grow trees ?

## Recursivity

A function can call itself: 
```js
function branch(startPoint, growthDirection, depth) {
  if(depth > maxDepth)
    return;
    
  let endPoint = growBranch(startPoint, growthDirection);
  line(startPoint[0], startPoint[1], endPoint[0], endPoint[1]);
  branch(endPoint, changeABit(growthDirection), depth + 1);
  branch(endPoint, changeABit(growthDirection), depth + 1);
}
```

---
class: nord-light

# Herboristery (cont.)

## L-Systems

General idea: create rules as strings. 
* Rules can "imply" other rules.
* Rules can encode "orders" to the drawing system.
  
Common conventions: 
* `F`: draw forward
* `+`: turn left
* `-`: turn right

Example of rules: 
```js
A = FA+FB-
B = AAB
```

Example: https://editor.p5js.org/jcelerier/sketches/XSWwqEyOs

---
class: nord-light

# Machinarium

How do we __create life__ ?

Cellular automaton rules.
- Another set of algorithmic rules that tend to create life-like patterns.
- Example: https://p5js.org/examples/simulate-game-of-life.html

General idea: grid of cells. 
- At each step, look at the cells around us.
- Depending to a set of rule, this will change the color of our cell for the next step.
- Very complex visual behaviours can arise from very simple rules.

---

class: nord-dark, center, middle

# Image processing

---

class: nord-light

# Image processing

It is possible to load and process images, to apply filters.
```js
let img;
function preload() {
	img = loadImage('assets/image.jpg');
}

function setup() {
  createCanvas(img.width, img.height);
  image(img, 10, 10); // Shows the image at position 10,10
}
```

---

class: nord-light

# Image processing (cont.)

Images are a matrix of pixels: each cell of the grid contains a color.

```js
function myFilter(img, x, y) {
  let col = img.get(x, y);
  let value = (red(col) + green(col) + blue(col)) / 3; 
  img.set(x, y, color(value,value,value));
}

function setup() {
  createCanvas(img.width, img.height);
  for(let j = 0; j < img.height; j++) 
    for(let i = 0; i < img.width; i++) 
      myFilter(img, i, j);

  img.updatePixels();
  image(img, 0, 0);
}
```
---

class: nord-light

# Image processing (cont.)

- Note that this holds for images created by Processing with createGraphics !
- Note also that you can access the main canvas's pixels with `get` / `set`.

Exercice: fill the canvas with pixels for the following function: 
```js
z = sin(x * y); 
```
This should look like this: 
https://www.wolframalpha.com/input/?i=sin%28x*y%29
Add a 0.01 * frameCount to the phase of the sin for a pretty result.
---

class: nord-dark, center, middle

# Generative typography

---

class: nord-light

# Generative typography

First approach (pixel-based): combining what we've seen so far.

- A character can be drawn on an offscreen texture (remember `createGraphics`).
- We can go check the pixels of that texture to see if they have been drawn into.
- We can use that to draw on our actual canvas, by introducing randomness, color changes, etc.

* Exercice: generate particles that move inside a character. The particles should not be able to leave the inside of the character. 

---

class: nord-light

# Generative typography (cont.)

Second approach (shape-based).

- p5.js provides the `textToPoints()` function: given 
  some text, it will return an array of [x, y] positions which delimitate the text.

* Exercice: use `textToPoints` and `line` to draw text manually.
* Exercice: distort the shapes randomly in an animated fashion, using Perlin noise.

