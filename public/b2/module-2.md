layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 2: motion design & interactivity

Jean-Michaël Celerier 

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>


---

class: nord-dark, center, middle

# Animation

---
class: nord-light

# Animation in p5.js

What we've been doing so far: 

```js
function setup() {
  // Drawn once
  line(0, 0, 100, 100); 
}
```

What we will be doing from now on:
```js
function setup() {
}
function draw() {
  // Drawn every 16 milliseconds.
  line(0, 0, 100, 100); 
}
```

---
class: nord-light

# Animation in p5.js (cont.)

```js
function setup() {
}
function draw() {
  // random() will return a new value every time.
  line(0, 0, random(0, 100), 100); 
}
```

Processing does not erase the canvas between successive calls to draw.

---
class: nord-light

# Animation in p5.js (cont.)

Solution: erasing the canvas.

Note: some artworks sometimes require *not* erasing the previously drawn frame. 

Ex. https://www.instagram.com/p/CHQM1szCyUn/ 
```js
function setup() {
}
function draw() {
  background(220);
  line(0, 0, random(0, 100), 100); 
}
```

---
class: nord-light

# Animation in p5.js (cont.)

Solution: erasing the canvas (partially).

Alpha-channel (transparency) allows to make previous drawings slowly fade out.

```js
function setup() {
}
function draw() {
  background(220, 220, 220, 50); // < "50" is the alpha value
  line(0, 0, random(0, 100), 100); 
}
```

---
class: nord-light

# Animation in p5.js (cont.)

It's too fast. 

```js
function setup() {
  frameRate(10); // call draw() 10 times per second
}
function draw() {
  background(220);
  // Now only drawn every 100 milliseconds.
  line(0, 0, random(0, 100), 100); 
}
```

---
class: nord-dark

# Free composition ! 

You've got 10 minutes :-)

Note: use lines ! rects ! circles ! ellipses ! randomness ! HSB colors ! everything we've seen so far !

---
class: nord-light

# Controlled animation
`frameCount` is the number of frames since the animation started.

```js
function setup() { }
function draw() {
  background(220, 220, 220, 200);
  line(0, 0, frameCount, 100); 
}
```

---
class: nord-light

# Cyclic animation
If we want smooth animations, a simple and efficient way is to use `sin` / `cos`.

```js
function setup() { }
function draw() {
  background(220, 220, 220, 200);
  // cos is between -1 and 1, we want it between 0, 1
  let percentage = 0.5 + 0.5 * cos(frameCount);
  line(0, 0, 100 * percentage, 100); 
}
```
Too fast ! 

---
class: nord-light

# Cyclic animation

```js
function setup() { frameRate(5); }
function draw() {
  background(220, 220, 220, 200);
  let percentage = 0.5 + 0.5 * cos(frameCount);
  line(0, 0, 100 * percentage, 100); 
}
```
Too janky ! 

---
class: nord-light

# Cyclic animation

```js
function setup() { }
function draw() {
  background(220, 220, 220, 200);
  let percentage = 0.5 + 0.5 * cos(0.01 * frameCount);
  line(0, 0, 100 * percentage, 100); 
}
```
purrfect ! 

---
class: nord-light

# Cyclic animation - alternative with modulo (%)

Using modulo will make evenly-spaced animation frames, which will loop back at zero every time.
```js
function setup() { }
function draw() {
  background(220, 220, 220, 200);
  line(0, 0, frameCount % 100, 100); 
}
```

---
class: nord-light

# Cyclic animation - pretty example

```js
function setup() { }
function draw() {
  background(220, 220, 220, 20);
  line(0, frameCount % 100, frameCount % 100, 100); 
}
```

Exercice: make it loop back to the top in a cyclic motion.

Hint: `if` - draw the domain on paper.

---
class: nord-light

# Exercise: spinner wheel

Make something similar to the various "loading" animations.

Hint : the coordinates of the circles should also be a circle.

Cheat sheet: `translate`, `rotate`.

Note that translation / rotation *are* reset across calls to `draw`, no need to call `push`, `pop`.

---
class: nord-dark, center, middle

# Interaction

---
class: nord-light

# Using the mouse position

`mouseX`, `mouseY` give the current position of the mouse cursor.

```js
function setup() { }
function draw() {
  background(220, 220, 220, 20);
  line(0, 0, mouseX, mouseY); 
}
```

---
class: nord-light

# Using the mouse position (cont.)

From the center: note that the mouse coordinates *aren't* affected by `translate` and thus must be corrected.

```js
function setup() { }
function draw() {
  background(220, 220, 220, 20);
  translate(width/2, height/2);
  line(0, 0, mouseX - width/2, mouseY - height/2); 
}
```

---
class: nord-light

# Using the mouse press (option A)

```js
function setup() { }
function draw() {
  background(220, 220, 220, 20);
  translate(width/2, height/2);

  if(mouseIsPressed) {
    stroke(255, 0, 0);
  } else {
    stroke(0, 0, 0);
  }

  line(0, 0, mouseX - width/2, mouseY - height/2); 
}
```
---
class: nord-light

# Using the mouse press (option B)

```js
function setup() { }
function draw() { }

// Called by p5 automatically when the mouse is pressed
function mousePressed() {
  ellipse(mouseX, mouseY, 100, 100);
}
```
---
class: nord-dark

# Free composition ! 

You've got 10 minutes :-)

---
class: nord-light

# Using the text function

```js
function setup() { }
function draw() {
  background(200, 200, 200, 40);
  let x = random(0, width);
  let y = random(0, height);
  rotate(random());
  // text must be put between single or double quotes
  text("hello world", x, y);
}
```

---
class: nord-light

# Using the keyboard

```js
function setup() { }
function draw() {
  background(200, 200, 200, 40);
}

// Called by p5 automatically when a key is pressed
function keyTyped() {
  let x = random(0, width);
  let y = random(0, height);
  text(key, x, y);
}
```

---
class: nord-dark

# See all the other input methods available in the reference

Section "Events".

p5 can work on mobile phone and use your mobile phone rotation, inclination, etc. 
Try to make something with it ! 

---

class: nord-dark, center, middle

# Particles

A good pretext to learn about boring computer science and derivatives.

---

class: nord-light

# Letting a circle live its life

```js
function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(220);  
  circle(100 + 10 * random(), 100 + 10 * random(), 50);
}
```

It cannot break free ! 

---

class: nord-light

# Letting a circle live its life (cont.)

```js
function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(220);  
  let x = 100 + 10 * random();
  let y = 100 + 10 * random(); 
  circle(x, y, 50);
}
```

We want to reuse `x` and `y` across calls to draw to make our circle move.

Solution: put them outside the function !

---

class: nord-light

# Letting a circle live its life (cont.)

```js
let x;
let y;
function setup() {
  createCanvas(400, 400);
  x = 100; 
  y = 100;
}

function draw() {
  background(220);  
  x += 0.5;
  y += 2; 
  circle(x, y, 50);
}
```
---

class: nord-light

# What if we have 12 circles ? 

```js
let x;
let y;
let x2;
let y2;
let x3;
let y3;
................................
NO.
```


---

class: nord-light

# Arrays

```js
let x = [], y = [], numCircles = 12;
function setup() {
  createCanvas(400, 400);
  for(let i = 0; i < numCircles; i++) {
    x.push(100); y.push(100);
  }
}
function draw() {
  background(220);  

  for(let i = 0; i < numCircles; i++) {
    x[i] += random(-1, 2);
    y[i] += random(-1, 1);
    circle(x[i], y[i], 50);
  }
}
```

---

class: nord-dark

# Array cheat sheet

```js
// Creation
let myArray = [];

// Adding a new element to an array
myArray.push(theNewElement);

// How long is my array ? 
console.log(myArray.length);

// Accessing the 1st element of an array
myArray[0]; // !!!!! we start at zero

// Accessing the 3rd element of an array
myArray[2];

// Accessing the last element of an array
myArray[myArray.length - 1];
```

---

class: nord-light

# Objects

Back to having a single circle.

Let's say we want to have strokes, fills, radiuses.

And let's say we also want to have rectangles.

```js
let circleX;
let circleY;
let circleRadius;
let circleStroke;
let circleFill;
let rectangleX;
let rectangleY;
let rectangleRadius;
................................
NO !
```

---

class: nord-light

# Objects: defining classes

```js
// This is the "definition" of a circle object.
class Circle {
  // Here we list all the attributes of the Circle object
  constructor() {
    this.x = 0;   this.y = 0;   this.radius = 50;
    this.stroke = color(255, 0, 0);
    this.fill = color(255, 0, 255);
  }

  // Here we say what happens when we want to draw it.
  // Note: the name can be whatever you want.
  draw() {
    stroke(this.stroke);    fill(this.fill);
    circle(this.x, this.y, this.radius);
  }
}
```
---

class: nord-light

# Objects: using classes

```js
let myCircle;
function setup() {
  createCanvas(400, 400);
  myCircle = new Circle();
  myCircle.x = 20;
  myCircle.y = 40;
}

function draw() {
  background(220);  

  myCircle.x += 1;
  myCircle.y += 1;

  // Here the name must be the same than the one you added in the Circle class.
  myCircle.draw();
}
```

---

class: nord-light

# Objects: complete example

https://editor.p5js.org/jcelerier/sketches/l_Lm153a7

---

class: nord-dark

# Class cheat sheet

```js
// Defining an object:
class MyClassName {
  constructor() {
    // Object attributes
    this.toto = 123;
    this.prettyColor = color(255);
  }
  // Class methods (functions)
  aFunction() {
    // ... whatever
  }
  anotherFunction() {
    // ... whatever
  }
}
```
---

class: nord-dark

# Object cheat sheet

```js
// Creating an object:
let myObject = new MyClassName();

// If the constructor takes arguments it's possible to pass them there
let myObject = new MyClassName(123, 456);

// Using the fields (attributes, members...) of an object: 
stroke(myObject.prettyColor);

// Changing the fields of an object
myObjet.toto = myObjet.toto * 2;

```


---

class: nord-light

# Combining arrays and objects to create animated particles

https://editor.p5js.org/jcelerier/sketches/QNDuawW_G

---

class: nord-dark

# Exercice

* Add a Rectangle class
* Add some rectangles in the `shapes` array to have varied shapes of particles.
---

class: nord-light

# Better for-loops when we use arrays.

```js
for(let myShape of shapes) {
  // ...
}

// is equivalent to
for(let i = 0; i < shapes.length; i++) {
  let myShape = shapes[i];
  // ...
}
```

---
class: nord-dark

# Free composition ! 

You've got 10 minutes :-)
