layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### reference

Jean-Michaël Celerier 

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-dark, center, middle

# Basic JS syntax

---
class: nord-light

# Comments

```js
// A single-line comment

/* A
   multi-line 
   comment
 */
```

---
class: nord-light

# Expressions (literals)

```js
// Numbers: 
1
3.5

// Strings
"hello world"
'hello world'
"#FF0000"

// booleans (truth values)
true
false
```

---
class: nord-light

# Expressions (operations)
```js
// Basic operations
a + b, a - b, a * b, a / b

// Advanced: modulo
a % b

// Advanced: exponentiation 
a ** b
```

---
class: nord-light

# Expressions (assignment)
```js
// Basic assignment to a variable
a = b;

// Basic operations
a += b, a -= b, a *= b, a /= b

// Advanced: modulo
a %= b

// Advanced: exponentiation 
a **= b
```

---
class: nord-light

# Expressions (comparison)

```js
a > b
a < b
a === b  // note the *three* equals ! 
a !== b  // means "is the value of 'a' different than the value of 'b'"
a >= b
a <= b
```

---
class: nord-light

# Variables

```js
// ok
let <name of the variable> = <an expression>;
let <name of the variable>;
<name of the variable> = <an expression>;

// for variables containing numbers specifically
let variable = 123;
variable = 456; // ok, variable is now 456
variable = variable + 2; // ok, variable is now 458
variable += 10; // ok, variable is now 468
variable ++; // ok, variable is now 469
variable --; // ok, variable is now 468
```

---
class: nord-light

# Expressions (function usage)
```js
// call a function
f()   

// call a function with two arguments
f(a, b) 

// call a function with nested calls
f(g(a), (b + c) * 2) 

// the above is equivalent to doing: 
let argument1 = g(a);
let argument2 = (b + c) * 2;
f(argument1, argument2);
```

---
class: nord-dark, center, middle

# Control flow (if, for)

---
class: nord-light

# Conditions (usage with if)

```js
// Conditions
if (<an expression that gives a truth value>) {

} else if (<another expression that gives a truth value>) {

} else {

}
```

the `else if` and `else` branches are optional. 
There can be as many `else if` as necessary.

---
class: nord-light

# Loops

```js
for (/* initialization */; /* loop condition */; /* incrementation */) {
    /* loop body */
}

// Example: count to 10
for (let i = 0; i < 10; i++) {
    console.log(i);
}

// Example: count to 10 by 2 (0, 2, 4, 6, 8)
for (let i = 0; i < 10; i += 2) {
    console.log(i);
}
```

---
class: nord-light

# Loops (cont.)

```js
// Example: count to 10 by 2 (0, 2, 4, 6, 8, 10)
for (let i = 0; i <= 10; i += 2) {
    console.log(i);
}

// Example: loop a random amount of times
let loopCount = random(5, 15);
for (let i = 0; i <= loopCount; i++) {
    console.log(i);
}

```

---
class: nord-light

# Loops (nested)

```js
for (let i = 0; i < 10; i++) {
    for (let j = 0; j < 10; j++) {
        console.log(i, j);
    }
}

// Will print: 
0 0
0 1
0 2
... etc
1 0
1 1
1 2 
... etc up to 
9 9 
```

---
class: nord-light

# Function declaration

```js
function myFunction() {
    let radius = random(10, 100);
    circle(0, 0, radius); // draw a random circle at coordinates 0, 0
}

// Arguments can be passed 
function myOtherFunction(x, y) {
    let radius = random(10, 100);
    circle(x, y, radius); 
}
```

---
class: nord-light

# Function declaration (cont.)

```js
// A function must be declared *before* being called in the source code
// not ok: 
function draw() { 
    myFunction(); 
}
function myFunction() {
}

// ok: 
function myFunction() {
}
function draw() { 
    myFunction(); 
}
```

---
class: nord-light

# Function declaration (cont.)

```js
// A function can perform a computation and return it 
// with the "return" keyword.
function mean(a, b) {
    return (a + b) / 2;
}

let result = mean(10, 20);
// result is 15
```

---
class: nord-light

# Variable scope

```js
// variables exist only within the pair of braces that encloses them: 

let myGlobal; // exists everywhere from this point.
function draw() {
    myGlobal = 0; // ok

    // myLocal will be accessible anywhere in the draw function from that point
    let myLocal = 0; // ok

    circle(myLocal, myLocal, 10); // ok

} // from this point myLocal doesn't exist anymore.

myLocal++; // error !
```

---
class: nord-light

# Variable scope (cont.)

```js
function draw() {
    let myLocal = 0; // ok
    // Scopes are be introduced with braces
    // This introduces a scope ;
    // the code executes normally, it's pretty much only cosmetic 
    // outside of the effect on local variables: 
    {
        let myOtherLocal = 2;
        myOtherLocal += myLocal; // ok, we can use myLocal and myOtherLocal here.
    } // from this point "myOtherLocal" does not exist anymore

    myOtherLocal++; // error ! 
    myLocal++; // ok, it's in the correct scope
}
```

---
class: nord-light

# Variable scope (cont.)

```js
function draw() {
    let myLocal = 0; // ok
    // Of course this holds also for if(), for(), etc...
    if (myLocal === 0) {
        let myOtherLocal = 2;
        myOtherLocal += myLocal; // ok, we can use myLocal and myOtherLocal here.
    } // from this point "myOtherLocal" does not exist anymore

    myOtherLocal++; // error ! 
}
```

---
class: nord-light

# Variable scope (cont.)

```js
function draw() {
    // In for-loops, the variable declared as part of the loop
    // is also part of the loop's scope and cannot be used outside.
    for(let i = 0; i < 10; i++) {
        let myOtherLocal = i;
    } // from this point both "i" and "myOtherLocal" don't exist anymore
    i++; // error !
    myOtherLocal++; // error ! 
}
```

---
class: nord-light

# Variable scope (cont.)

```js
function myFunction(x, y) {
    circle(x, y, 100);
}
function draw() {
    let x = 10;
    let y = 10;
    // Attention: the "x", "y" variable names used here are
    // unrelated to the names chosen on the 
    // myFunction declaration ! We chose the same name 
    // only to make the code more readable.
    myFunction(x, y); 

    // Could also be the following for instance
    myFunction(x + 100, y / 2);
}
```

---
class: nord-light

# Variable scope (cont.)

```js
// z does not exist here
function myFunction(x, y) {
    let z = x + y;
    // z exists here
}
// z does not exist here
function draw() {
    // z does not exist here
    myFunction(x, y); 
    // z does not exist here
}
```

---
class: nord-dark, center, middle

# Advanced syntax: arrays

---
class: nord-light

```js
// Creation
let myArray = [];

// Adding a new element to an array
myArray.push(theNewElement);

// How long is my array ? 
console.log(myArray.length);

// Accessing the 1st element of an array
myArray[0]; // !!!!! we start at zero

// Accessing the 3rd element of an array
myArray[2];

// Accessing the last element of an array
myArray[myArray.length - 1];
```

---
class: nord-dark, center, middle

# Advanced syntax: classes

---
class: nord-light

```js
// This is how a class is defined:
class NameOfTheClass {
  // This special function, "constructor" is called when we create an object of the class. 
  // All other names can be arbitrary.
  constructor() {
      // Refer to class members with the "this" keyword.
      this.memberName = 123;
  }

  // Add methods:
  doStuff() {
    circle(this.memberName, this.memberName, 100);
  }

  doOtherStuff(someArgument) {
    for(let i = 0; i < someArgument; i++) {
      this.doStuff();
    }
    return someArgument * 2;
  }
}
```

---
class: nord-light

```js
// Create an instance of a class : an object.
// An object is like a box which contains all the variables and functions defined in the class.
// A variable allows to access the content of the box and do stuff with it.

// Calling "new <A class name>" calls the special "constructor" function 
// defined in the class.
let myObject = new NameOfTheClass();

// Access its members
myObject.memberName = 123;
console.log(myObject.memberName);

// Call its functions
myObject.doStuff();
let result = myObject.doOtherStuff(123);
// result == 456;
```

---
class: nord-light

```js
// This is the "definition" of a circle object.
class Circle {
  // Here we list all the attributes of the Circle object
  constructor() {
    this.x = 0;   this.y = 0;   this.radius = 50;
    this.stroke = color(255, 0, 0);
    this.fill = color(255, 0, 255);
  }

  // Here we say what happens when we want to draw it.
  // Note: the name can be whatever you want.
  draw() {
    stroke(this.stroke);    
    fill(this.fill);
    circle(this.x, this.y, this.radius);
  }
}
```

---
class: nord-light

```js
let myCircle;
function setup() {
  createCanvas(400, 400);
  // Note: it is a good practice to create object instances inside the
  // setup() function.
  myCircle = new Circle();
  myCircle.x = 20;
  myCircle.y = 40;
}

function draw() {
  background(220);  

  myCircle.x += 1;
  myCircle.y += 1;

  // Here the name must be the same than the one you added in the Circle class.
  myCircle.draw();
}
```


---
class: nord-light
# Lambda-Functions

```js
class MyClass {
  constructor() { this.x = "set in constructor()"; }
  update() { this.x = "set in class-defined update()"; }
}
function setup() {
  let p = new MyClass(); console.log(p.x);
  
  // This calls the "update()" defined in the class
  p.update(); console.log(p.x);
  
  // Now we replace the "update()" function in the class by a custom one
  p.update = () => { 
    p.x = "set in lambda function"; 
  };
  
  // From now on p.update() will refer to the small fragment of code defined above, 
  // called a lambda-function
  p.update(); console.log(p.x);
}
```