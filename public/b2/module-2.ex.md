layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 2.1: exercices

Jean-Michaël Celerier 

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light

# Exercice 11: array practice

1. Draw 5 circles, from coordinates in two x/y arrays. e.g. 

```js
let x = [10, 345, ...];
let y = [460, 1, ...];
```

2. Do the same but with the coordinates of the circle stored in a single array: 

```js
let circles = [ [10, 460], [345, 1], ... ];
```

3. Add the circle color to the coordinates array.

---
class: nord-light

# Exercice 12: class practice

Now let's replace our arrays by objects.

Given: 
```js
class Circle {
  constructor(x, y, color) { 
    this.x = x;
    this.y = y;
    this.color = color;
  }
}
```

Replace the arrays in the previous exercise by `Circle` objects.

---
class: nord-light

# Exercice 13: class practice (cont)

Add a new Rectangle class, and add some objects in a new array: 

```js
let circles;
let rectangles; 
...
```

---
class: nord-light

# Exercice 14: particles

Particles are small entities that move according to physical laws.

* Create a class to represent particles: the shape you use is free.
  * Choose the attributes of your particles.
  * Add a `draw()` method to draw a particle.
  * Add an `update()` method to change the position of the particle.
 
* Create an instance of the particle, make it move regularly in a fixed direction, 
by updating it and drawing it in the p5 `draw()` function.
* When the particle goes outside the screen, you can either choose to repulse it 
towards the inverse direction like a ball bouncing against a wall, or 
make it reappear on the other side of the canvas.
* Now create an array of particles instead.

---
class: nord-light

# Exercice 15: particles (cont.)

Particles can have interesting motions : we can steer them towards
a path defined by an external force.

1. Recall what is the parametric pattern of a circle.
2. Create particles positioned on the circle.
3. Make them move randomly.
4. For a nice effect, experiment with blend modes.
5. Notice that the particles tend to diverge. 
   How can we make them stay around the shape of the circle ?


---
class: nord-light

# Exercice 16: particles (cont.)

* Now that our particles stay put, let's try to put them at a different place when they are created: 
  randomize their initial position across the canvas. Try it with various blend modes, such as OVERLAY, etc.
* Make the particles follow other kinds of patterns: for instance a sine wave or a spiral.

Now we want to have multiple shapes on screen, each represented by particles.
Let's model those by classes too.

* Create a Shape class which contains an array of particles, and a shape that those particles are supposed to follow.
* Create multiple shapes like this, and make the shapes themselves move randomly.

---
class: nord-light

# Exercice 17: repulsing particles

* Create an array of random particles moving across the screen.
* When we move the mouse across the screen, we want to repulse the particles that are close to the mouse outside of a circle. The closer the particle is from the mouse, the faster it should go away.

Hint: the `dist()` function gives the distance between two points.

---
class: nord-light

# Exercice 18: exam !

For sunday 29/11/2020, 23:59 CET, by mail at jeanmichael.celerier@ynov.com.

* We want to make an effect similar to for instance the background here, with the particles and the triangles: https://whois.domaintools.com/

1. Describe textually how you would go about solving this.
2. Implement a non-interactive, non-animated Plexus effect.
3. Make the particles move.
4. Add some interactivity: 
* Either making the particles & lines appear when the mouse is moving over them.
* Or displacing the particles when clicked.