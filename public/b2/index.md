layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light
<br/><br/>

Module 1: [Courses](module-1.html) / [Exercices](module-1.ex.html) / [Corrections](module-1.corr.html)

------

Module 2: [Courses](module-2.html) / [Exercices](module-2.ex.html) / [Corrections](module-2.corr.html)

------

Module 3: [Courses](module-3.html) / [Exercices](module-3.ex.html)

------

Module 4: [Courses](module-4.html) / [Exercices](module-4.ex.html)

------

[Module 5 (project)](module-5.html)

------

[Reference](reference.html) / [Practice](practice.html)
