layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 1.2: exercices

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light

# Exercice 1 & 4: drawing a sun

```js
function setup() {
  createCanvas(400, 400);
  // Choose the colors
  background(20);
  stroke("yellow");
  fill("yellow");

  // Put the drawing origin in the center of the canvas
  translate(width / 2, height / 2);

  // First draw the rays
  let rayLength = 100;
  let rayCount = 50;
  for(let i = 0; i < rayCount; i++) {
    // Each ray will be rotated a bit
    rotate(2 * PI / rayCount);
    line(0, 0, rayLength, 0);
  }

  // Then draw the center circle
  circle(0, 0, 100);
}
```

---
class: nord-light

# Exercice 2: drawing parallels
```js
function setup() {
  background(250);

  // The lines will fit a rectangle that starts at coordinates (10; 20),
  // and is 90 pixels wide.
  for(let i = 0; i < 5; i++) {
    line(10, 20 + i * 7, 100, 20 + i * 7);
  }
}
```

---
class: nord-light

# Exercice 3: recreating an artwork

Correction here: https://editor.p5js.org/jcelerier/sketches/wbfZhBtJ2

---
class: nord-light

# Exercice 5: back to the sun, but harder

```js
function setup() {
  createCanvas(400, 400);

  background(20);
  stroke("yellow"); fill("yellow");

  translate(width / 2, height / 2);

  // First draw the rays
  let rayLength = 100;
  let rayCount = 50;
  for(let i = 0; i < rayCount; i++) {
    // Each ray will be rotated a bit
    let theta = 2 * PI * i / rayCount;
    let x = rayLength * cos(theta);
    let y = rayLength * sin(theta);

    line(0, 0, x, y);
  }

  // Then draw the center circle
  circle(0, 0, 100);
}
```

---
class: nord-light

# Exercice 6: many suns
```js
function sun(x, y, col, rayLength) {
  push();
  translate(x, y);
  stroke(col); fill(col);

  let rayCount = 50;
  for(let i = 0; i < rayCount; i++) {
    rotate(2 * PI / rayCount);
    line(0, 0, rayLength, 0);
  }

  circle(0, 0, 100);
  pop();
}

function setup() {
  createCanvas(400, 400);
  background(20);

  sun(30, 30, "yellow", 100);
  sun(250, 330, color(255, 120, 0), 100);
}
```

---
class: nord-light

# Exercice 7: random suns
```js
function sun(x, y, col, rayLength) {
  push();
  translate(x, y);
  stroke(col); fill(col);

  let rayCount = 50;
  for(let i = 0; i < rayCount; i++) {
    rotate(2 * PI / rayCount);
    line(0, 0, rayLength * random(0.9, 1.1), 0);
  }

  let diameter = random(20, 0.9 * rayLength);
  circle(0, 0, diameter);
  pop();
}

function setup() {
  createCanvas(400, 400);
  background(20);

  sun(30, 30, "yellow", 100);
  sun(250, 330, color(255, 120, 0), 100);
}
```
---
class: nord-light
# Exercice 8: more random suns

* Change the sun program to use the HSB colorspace.

  => https://editor.p5js.org/jcelerier/sketches/AI0f_q4oa

* Make one in two rays of the sun brighter.

  => https://editor.p5js.org/jcelerier/sketches/PRxtRwgQ6

* Make on average one in two rays of the sun brighter by using `random()` for the condition.

  => https://editor.p5js.org/jcelerier/sketches/KBxxAMuOv

---
class: nord-light

# Exercice 9

https://editor.p5js.org/jcelerier/sketches/u0Ig5MMVG

---
class: nord-light

# Exercice 10

https://editor.p5js.org/jcelerier/sketches/kdPMEwGob