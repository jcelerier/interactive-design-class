layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 1.1: exercices

Jean-Michaël Celerier 

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light

# Before starting

- Install Visual Studio Code (or update it to the latest version in the Help menu)
- Install p5.vscode extension
- Create a new project: F1 (or ⌘+Shift+P on Mac) and type "Create p5.js Project"
- The code will be in the `sketch.js` file
- Press "Go Live" at the bottom right of the VSCode window to open a browser which will show your work.

---
class: nord-light

# Back to basics

# Exercice 1: drawing a sun

1. Draw a sun in the center of the canvas. 
2. Make it so that the sun is always centered, no matter the size of the canvas.
3. Make it so that the ray lengths can change with a single line of code.
4. Save it, we will come back to it later !

Cheat sheet: `circle`, `line`, `stroke`, `fill`, `translate`, `width`, `height`.

---
class: nord-light

# Exercice 2: drawing parallels
- Open a new sketch.
- Draw 5 horizontal or vertical lines of the same length next to each other, spaced by 7 pixels each.

Cheat sheet: `line`, `for`.

---
class: nord-light

# Exercice 3: recreating an artwork
1. Reproduce something similar to 
https://twitter.com/xiiixiii/status/1318868653488656384/photo/3
2. Save it, we will come back to it later !
   
Cheat sheet: `ellipse`, `rect`, `createCanvas`, `background`.

---
class: nord-light

# Exercice 4: back to the sun

* Make the number of rays of the sun a variable. The rays must be evenly spaced.
* Hint: The total angle of a circle is `2 * PI` (or `TAU`).

Cheat sheet: `for`, `rotate`, `PI` (or `TAU`).

# Exercice 5: back to the sun, but harder
* Do the same than exercice 4 but without using `rotate`.

Cheat sheet: `sin`, `cos`, [this visual guide](https://editor.p5js.org/jcelerier/sketches/acPp467og)

---
class: nord-dark

# Functions

```JS
function theNameYouWant(argument1, argument2, [etc etc...]) {
  // the code of the function
}
```

```JS
let myVariable = 123;
function myFunction(argument1) {
  console.log(myVariable); // prints 123
}
function myOtherFunction(myVariable) {
  console.log(myVariable); // prints what's in the argument
}
```

---
class: nord-light

# Exercice 6: many suns

1. Put the drawing of the sun in a function. 
   * Pass the position, the color and the ray length as arguments.
2. Draw a lot of suns :-)

Cheat sheet: `push`, `pop` for keeping the translation inside the function.
---
class: nord-dark

# Introducing randomness

* `random()`: returns a random number between 0 and 1.
* `random(10, 20)`: returns a random number between 10 and 20.

---
class: nord-light

# Exercice 7: random suns

* Add a bit of randomness to the length of the sun rays
* Add a bit of randomness to the size of the suns.

---
class: nord-dark

# Introducing color spaces

* `colorMode(HSB, 1, 1, 1);`: from now on, fill / stroke / background all take 
   colors in Hue, Saturation, Brightness coordinates instead of Red, Green, Blue coordinates.
* See http://colorizer.org/

---
class: nord-light
# Exercice 8: more random suns

* Change the sun program to use the HSB colorspace.
* Make one in two rays of the sun brighter.
* Make on average one in two rays of the sun brighter by using `random()` for the condition.

Cheat sheet: 

```JS
if(i % 2 == 0) 
{
  /* i is even */
} 
else
{ 
  /* i is odd */ 
}
```

---
class: nord-light

# Exercice ??? 
10 minutes: free creation, it must not look like anything we've been doing today :-)

---
class: nord-light

# Exercice 9 (generalizing exercice 3)

* Make a program that generates random artworks in the same style than exercice 3.
* Hint: use functions for the various groups of shapes ! 

---
class: nord-light

# Exercice 10 (exam !)

* Make a program that generates artworks in the following style: 
* Send me the result by mail by sunday 15/11/2020 23:59 UTC+1.
  
  ![circle](static/circle-small.png) 
  ![circle](static/circle2-small.png) 
  ![circle](static/circle3-small.png)

Note: first write down what are the steps to follow, then write the code. 
(Send me both things in the mail !)

Strongly inspired by https://www.instagram.com/entropismes
