layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 4: web integration, exploration of the field

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---

class: nord-light

# Ex. 27 - Portfolio !

Let's start making a visual portfolio for your works.

1. Take two of your previous sketches (use sketches without mouse interaction), and put them in a single web page. Remember: we need to use the p5 instance mode for that.
**Keep the .js files separate !**

2. It is possible to open a link from P5 code with:
`window.open('http://example.com/foo.html', '_self')`.
Modify your sketch so that clicking on it in the portfolio opens it in full screen.
Note: all the sketches will receive the mouse press events. We have to check whether the click 
happens inside the right sketch.

3. Use a global variable in the HTML page to make sure that it is not possible to click again on the sketch once it is in fullscreen (or that clicking does an actual interaction with the sketch).

---

class: nord-light

# Ex. 28 - Charts

Take the following data source:
`https://www.reddit.com/r/science.json`

1. Make a plot of the links, where the x axis is the index, and the y axis the number of upvotes (look for the "ups" key in the dataset - remember, use Firefox !).

2. Make the plot interactive: whenever the mouse hovers a data point, display a small circle and a cross centered on that point.

---

class: nord-light

# Ex. 29 - Charts (2)

Advanced interactivity: display the thumbnail above it if there is one.

To do so, we likely want to load the images asynchronously, and store them in a small class which contains the data point and the image data.

Hint:
```js
i = ... current datapoint index ...;
loadImage(
  'https://the.url/image.jpeg',
  img => { dataPoint[i].image = img; }
);
```


---

class: nord-light

# Ex. 30 - Charts (3)

1. Reload the data source regularly. It is possible to get the current elapsed milliseconds with `millis()`.
We likely want to use the asynchronous version of the `loadJSON` function.

2. Instead of directly replacing the entire graph, we want things to move and change smoothly if possible.
How would you do that ?


---

class: nord-light

# Ex. 31 - Let's breath for a bit :-)

https://genuary2021.github.io/prompts#jan8

---

class: nord-light

# Ex. 32 - Controls

1. Use an API to fetch a set of images (example: https://shibe.online/).

2. Interpolate from one image to another.

3. Allow to choose the amount of interpolation with a slider.

4. Allow to choose the blend mode with radio buttons or a combo box.
