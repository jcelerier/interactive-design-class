layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 3: advanced techniques

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>


---

class: nord-light

# Ex. 19 - Shapes and noise

Note: put every question in a separate function, wrapped by push / pop.

1. Draw a square with `vertex`, from four points.
2. Draw a line with `vertex`.
3. Draw a square with four lines. Note that we can't really use the custom line function we wrote.
   But it is possible to simplify it.

4. Add some randomness to the line drawing, to make the square wobbly.

Hints: `vertex`, `lerp`

---
class: nord-light

# Ex. 20 - Advanced shapes

1. Create a shape by storing the successive mouse positions
in an array when the mouse is pressed. Draw the in-progress shape with a stroke.
When the mouse is released, fill the shape.

2. Extend this code to support multiple shapes: when the mouse is released, it will store the shape being created somewhere.
   Hint: you can store arrays in arrays.

3. Instead of just storing positions, create a Shape class with an associated fill color, and store that.



---
class: nord-light

# Ex. 21 - Noise

1. Draw a circle with some noise added for the coordinates.
2. Increase the amount of points to make flower-like things.


---
class: nord-light

# Ex. 22 - Mechanics

1. Create a simple particle system: a new particle should be added when clicking somewhere on the screen.
2. Recall what are the three main important quantities when simulating movement.
3. Note that the gravitation force is expressed as follows, between two points A, B:

```matlab
F(a,b) = -G * mass(A) * mass(B) * norm(A, B) / distance(A, B)²
```

Create a new, separate particle, and apply the formula for all the particles created through a click to this particle.

Note: for the variables, good-looking values are:
* G = 0.0628
* Mass of the "attractor" particles: between 1000 and 100000.
* Mass of the "moving" particle: between 10 and 1000.


---
class: nord-light

# Ex. 23 - 3D Mechanics

1. Refactor the particles with classes if done with arrays.

Hint: you can create "inline" objects in JS, with the following syntax:
```js
let obj = {x: 123, y: 456, z: 789, mass: random(10, 1000)};
```

2. Let's move to 3D ! Add a z coordinate to the attractors and the particles, chosen randomly between for instance -500, 500 for the former.
3. To make things pretty: `noStroke()`, `lights()`.
4. To have a more dynamic scene, start the drawing with this:
```js
rotateX(0.01 * frameCount);
rotateY(0.01 * frameCount);
rotateZ(0.01 * frameCount);
```
---
class: nord-light

# Ex. 24 - Images

1. Load and display an image of your choosing (not too big, around 500x500).
2. Make the image grayscale.
3. Show only the red component of the image.

---
class: nord-light

# Ex. 25 - Images 2 - glitch art

Pixel sorting is a technique that gives a very fun effect: http://datamoshing.com/2016/06/16/how-to-glitch-images-using-pixel-sorting/

1. Note that to sort an array one can do

```js
let array = [];
...;
array.sort((a, b) => a - b); // a, b will be elements of the array
```

2. Note that to create an object "in-line", one can do

```js
let myObject = { brightness: 123, col: c };
```

3. Implement a primitive pixel sorting: for each line of a picture, sort all the pixels
   by their brightness

---
class: nord-light

# Ex. 25 - Typography 1

1. Render a character on a canvas.
2. Generate particles that move inside the character.
3. Have the particles leave some trails for a nice effect, play with alpha and blend modes.

---

class: nord-light

# Ex. 26 - Typography 2

1. Render a character's outline with `textToPoints`, `vertex`, `beginShape`, etc.
2. Morph a character into another.

