layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 4: web integration, exploration of the field

Jean-Michaël Celerier 

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---

class: nord-dark, center, middle

# Web embedding

---

class: nord-light

# Control interfaces

To control parameters accurately, instead of setting them by hand in the code,
we can use HTML user interface elements such as sliders, etc. in order to control 
them live.

```js
let slider;
function setup() {
  slider = createSlider(0, 255, 100);
}

function draw() {
  background(slider.value());
}
```

---

class: nord-light

# Control interfaces (cont.)

Creating and editing an HTML label:

```js
let myLabel;
function setup() {  
  myLabel = createSpan();
}

function draw() {  
  myLabel.elt.innerHTML = " value: " + mouseX;
}
```
---

class: nord-light

# Small exercice

- Go look for `createButton` on the p5 docs.
- Create a sketch that draws a random element (circle, rectangle, ...) whenever 
  the button is pressed.
- Control the size of the created elements with a slider.

---

class: nord-light

# Embedding p5.js in a web page

Let's look at how the sketches created by VSCode work: 

* sketch.js: our sketch
* index.html: the web page
* style.css: web page styling

Include the p5 libraries:
```html
<head>
  <script src="libraries/p5.min.js"></script>
  <script src="libraries/p5.sound.min.js"></script>
</head>
```

---

class: nord-light

# Embedding p5.js in a web page (cont.)

Include the script in the web page body.
```html
<body>
  <script src="sketch.js"></script>
</body>
```

---
class: nord-light

# Embedding p5.js in a web page (cont.)

What if we want to use multiple sketches in a single web page ? 
Needs to be more explicit: each sketch will live inside a function.

```js
var s = function(p) {
  p.setup = function() {
    p.createCanvas(400, 200);
  };
  p.draw = function() {
    p.background(0);
  };
};
var sketch1 = new p5(s, 'firstCanvas');
```

---
class: nord-light

# Displaying external data from APIs

Multiple ways: 
* Reading from a file
* From a live web API using WebSockets

Most of the time, APIs use a well-specified data format, generally JSON.

---
class: nord-light

# Displaying external data from APIs (json example)

```json
{"menu": 
 {
  "id": "file",
  "value": "File",
  "popup": {
    "menuitem": [
      {"value": "New", "onclick": "CreateNewDoc()"},
      {"value": "Open", "onclick": "OpenDoc()"},
      {"value": "Close", "onclick": "CloseDoc()"}
    ]
  }
 }
}
```

---
class: nord-light

# Displaying external data from APIs 

p5.js can easily open JSON files.

JSON stands for : `JavaScript Object Notation`.
JSON data can be converted instantly into JavaScript objects ! 

```js
let myData;
function preload() {
  myData = loadJSON('assets/myData.json');
}
function setup() {
  // example !
  for(let item of myData.menu.popup.menuitem) {
      console.log(item.value);
  }
}
```


---
class: nord-light

# Displaying external data from APIs (cont.)

Kinds of data visualizations:
* Graphs
* Bar charts
* Map

Case study: US elections 2020: 

https://medium.com/swlh/data-visualization-us-election-2020-a3d3531b611c

Book: Data Visualisation: A Handbook for Data Driven Design


---
class: nord-light

# Displaying external data from APIs (cont.)

All the techniques we've seen so far can be used in a data-driven context: 

* Instead of using functions defined in parametric terms, use functions made of data.
* Use data tables, create functions to interpolate on that data.
* Linear interpolation is a basic one for 1D variables.
* Spline, Bézier, various more advanced functions.
* 2D: Bilinear interpolation, etc etc.


---
class: nord-light

# Displaying external data from APIs (cont.)

Let's see how that looks in practice.

- Take a JSON with interesting data from 

https://github.com/jdorfman/awesome-json-datasets

- Make a plot of the data.

---
class: nord-dark, center, middle

# Other systems & libraries

---
class: nord-light

# Examples:
- https://animejs.com/
- mojs
- d3.js
- OpenFrameworks, Max/MSP, TouchDesigner, Processing...

---
class: nord-light

# Declarative example

Object declaration syntax in Javascript: very similar to JSON.

```js
let myObject = {
  variable1: 123,
  variable2: ['foo', 'bar'],
  variable3: { 
    someOtherObject: { 
      someArray: [ { x: 123, y: 456 }]
    }
  }
}
```

---
class: nord-light

# Declarative example: mojs

- Website: https://github.com/mojs/mojs
- API documentation: https://github.com/mojs/mojs/tree/master/api
- Tons of simple and beautiful common animation patterns implemented.
- Can be composed together


---
class: nord-light

# Declarative example: d3.js

- Toolkit for data visualization
- Website: https://d3js.org/
- Examples: https://observablehq.com/@d3/stacked-to-grouped-bars
- Much more examples: http://techslides.com/over-2000-d3-js-examples-and-demos

---
class: nord-light

# Imperative example: Processing

- https://processing.org
- The original "version" of p5.js
- Almost all the functions have the same name.
- But variables have types: `int`, `float`, etc. 
- `int i = 123;` instead of `let i = 123;` 
- `void foo(int x) { ... }` instead of `function foo(x) { ... }`   

---
class: nord-light

# Semi-imperative example: OpenRNDR

- https://openrndr.org/
- Very similar to Processing, but written in Kotlin.
- Small but growing community, has good chances of becoming the next p5.

---
class: nord-light

# Imperative example: OpenFrameworks

- https://openframeworks.cc/
- Similar to Processing, but written in C++.
- More performant language.
- More complicated to write.  

---
class: nord-light

# Semi-imperative example: Nannou

- https://nannou.cc/
- API similar to OpenRNDR.
- Written in Rust, very hype language.
- Even more complicated to write than OpenFrameworks.
- Counterpart: the compiler is able to catch a very large amount of errors.  

---
class: nord-light

# Patch example: PureData

- https://puredata.info/
- Graphical patching language.
- Mostly for audio.

---
class: nord-light

# Patch example: Max/MSP

- https://cycling74.com/
- Graphical patching language.
- Originally for audio but now can also do some level of graphics.


---
class: nord-light

# Patch example: vvvv

- https://vvvv.org/
- Graphical patching language.
- Can be scripted in C#.
- Windows-only.
- Comes with a *lot* of features out of the box: Kinect, AI, etc.

---
class: nord-light

# Patch example: TouchDesigner

- https://derivative.ca/
- Graphical patching language.
- Can be scripted in Python.
- Made mostly for visuals.
- Comes with a *lot* of features out of the box: Kinect, AI, etc.

---

class: nord-dark, center, middle

# Shaders

---

class: nord-light

# Shaders

- So far all the drawings we have been doing have been computed by the CPU: 
we haven't used the graphics card at all ! 
- Graphics card use specific programming languages: shaders.
- They describe the operation that the GPU does on every pixel.

---

class: nord-light

# Shaders (cont.)

- Most common language: GLSL
- Playground / sandbox: https://www.shadertoy.com/
- Edit examples here: https://thebookofshaders.com/edit.php 

```glsl
void main() {
  gl_FragColor = vec4(1, 0, 0, 1);
}
```

---

class: nord-light

# Shaders (cont.)

- GLSL is much more restricted than JavaScript.
- It is a typed language, like Processing: 
  * In javascript, variables for numbers, array, etc... are all declared with let.
  * In GLSL, we have to specify the types of the variables: numbers (int, float) and arrays of fixed size
    (vec2, vec3, vec4)
  * The GLSL compiler will give type errors if a value is assigned to the wrong type
- Very GLSL-specific: array syntax.
  * 3D point: xyz coordinates. 
  * On a value of type vec3, it is possible to access coordinates: 
    `vec3.x`, `vec3.y`, `vec3.z` (yields a float)
  * But also `vec3.xy` (vec2), `vec3.rgb` (vec3), `vec3.bgr` (vec3) 
  * and any combination thereof.


---

class: nord-light

# Shaders (cont.)

- No simple functions like circle, ellipse, etc. in shaders: 
  a dataflow is defined, which gives the value of a pixel.

- To draw a circle, one must write the function that given specific coordinates, 
  yields that circle (keyword: signed distance field).
- Most commonly textures are used for that kind of thing, as this is very computationally expensive.

- On the other hand: extremely powerful given a strong GPU.

