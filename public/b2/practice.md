layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### practice exercices

Jean-Michaël Celerier 

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-dark

# Fundamentals

- All the exercises must run without warnings.
- Code must be linted / prettified.
- All the exercices must be correct with any canvas size (except absurdly small), unless specified otherwise. 
- All the exercices (separated by numbers, 1., 2., etc) are independent.

---
class: nord-light

# Geometry I

1. Draw circles at each corner of the canvas.
  * The border of the circles must touch the borders of the canvas.
  * The radius of the circles must be set through a variable that can be changed.

2. Draw circles in a cross pattern using loops. The circles must touch but not intersect.

3. Draw circles in the shape of a circle: 
  * First by using `rotate`.
  * Then by specifying their coordinates manually.

---
class: nord-light

# Geometry II

1. Draw a triangle which fills the canvas.
  * Then draw a triangle that fills half of this triangle.
  * Then another smaller triangle that fills half of this one.
  * Do so using a loop, so that you can fill with as many triangles you want.

---
class: nord-light

# Patterns I

1. Draw a grid of 10*10 squares which fills a canvas.

2. Draw a grid where squares are 20 * 20 ; the grid must go as far as the canvas does no matter its size.

3. Draw a grid of 3 * 3 squares centered in the canvas. The squares must have some margins and some padding.
  * In each element of the grid, print some text; maybe a poem of your liking.

---
class: nord-light

# Patterns II

- Draw waves, with the sin function.
- Make the waves move by adding a phase which depends on the frame count.
- Make the waves move each by a different speed, by maintaining an array where the phase of each wave is saved.
- Rewrite your code by using a class for the waves. 

 
---
class: nord-light

# Patterns III

- Put the code for drawing a circle in a function (don't call it `circle` as it will conflict with p5's function !).
- Draw circles of various radii and shapes in a grid.
- To make things prettier, consider putting each circle inside a small frame-like rectangle like in Patterns I.
- Make it so that the canvas is always filled with random circles no matter its size.

Cheat sheet: 

```js
// Try this:
for(let i = 0; i < 10; i++) {
  for(let j = 0; j < 10; j++) {
    console.log(i, j);
  }
}
```

---
class: nord-light

# Happiness separation

- Create an array of random values, and fixed size. To do so, when the array reaches the maximum size you set, for instance "100", call `shift()` to remove the first element.
- Plot the values of this array over the horizontal axis, by using `vertex`.
  * Note: the plot can be made smoother by using `curveVertex` instead of `vertex`.
- Apply a smoothing distribution to the array of values so that the values at the left and right side of the curve are 0, while the values at the middle of the curve are unchanged. This can be done easily by multiplying the `i`th value of the array by `exp(-f(i)*f(i))` (see https://www.wolframalpha.com/input/?i=exp%28-x%C2%B2%29), with `f` being a function that maps `i` which is between 0 and N, to the [-1, 1] interval. 
- Try to reproduce this famous album artwork: https://www.amazon.fr/Pleasures-Joy-Division/dp/B00XILAIWI



---
class: nord-light

# Parametricity I

1. Plot the `y = x` mathematical function, by using small line segments.
2. Do the same for `y = sin(x)`.
3. Do the same for `y = tan(x)`.

4. Plot the `z = y * x` 2D function. Instead of using 3D we will use lightness to indicate the `z` value.
5. Plot the `z = cos(y * x)` 2D function in the same way.

---
class: nord-light

# Parametricity II

1. Draw a parametric circle by drawing line segments from one point to another. 
  * Parametrize the number of points from numbers going from 3 to 10, what do we see ?
2. Instead of using `line` in the previous exercise, do it with `beginShape`, `vertex`, `endShape`.

Formula of the circle of radius r: 

```js
let x = r * cos(theta);
let y = r * sin(theta);
```

with theta between `0` and `2 * PI`.

---
class: nord-light

# Parametricity III: Lissajoux

The Lissajoux curves have the following parametrization: 
  
```js
let x = a * sin(b * t);
let y = c * sin(d * t + phi);
```

with a, b, c, d, phi more-or-less random parameters, and t between `0` and `2 * PI`. 
* Draw a Lissajoux curve with `a = 1, b = 2, c = 1, d = 2, phi = 0`.
* Make a function to draw a Lissajoux curve with arbitrary parameters.
  (again, use `beginShape`/`vertex`/`endShape`)
* Find some pretty ones :-)

---
class: nord-light

# Parametricity IV: Lissajoux

* Reproduce an artwork akin to the next slide.

---
class: nord-light
background-image: url(static/lissajoux-bg.png)

---
class: nord-light

# Parametricity V: Spirals

* Find the parametrization of the spiral yourself by researching it on the internet.
* Draw various kinds of spirals - put the code in a function.