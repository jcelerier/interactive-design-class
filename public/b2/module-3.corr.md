layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 3: advanced techniques

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>


---
class: nord-light

# Ex. 19.1

```js
function vertexSquare(x, y, side)
{
  push();
  translate(x, y);
  beginShape();
  vertex(0, 0);
  vertex(0, side);
  vertex(side, side);
  vertex(side, 0);
  endShape(CLOSE);
  pop();
}
```

---
class: nord-light

# Ex. 19.2

```js
function vertexLine(x1, y1, x2, y2)
{
  beginShape();
  let numPoints = 10;
  for(let i = 0; i < numPoints; i++) {
    let x = lerp(x1, x2, i / numPoints);
    let y = lerp(y1, y2, i / numPoints);

    vertex(x, y);
  }
  endShape(CLOSE);
}
```

---
class: nord-light

# Ex. 19.3

```js
function vertexLine(x1, y1, x2, y2)
{
  let numPoints = 50;
  for(let i = 0; i < numPoints; i++) {
    let x = lerp(x1, x2, i / numPoints);
    let y = lerp(y1, y2, i / numPoints);
    vertex(x, y);
  }
}

function myNewSquare(x, y, side)
{
  push();
  translate(x, y);
  beginShape();
  vertexLine(0,0,side,0);
  vertexLine(side, 0, side, side);
  vertexLine(side, side, 0, side);
  vertexLine(0, side, 0, 0);
  endShape(CLOSE);
  pop();
}
```

---
class: nord-light

# Ex. 20.1

---
class: nord-light

# Ex. 20.2

---
class: nord-light

# Ex. 20.3


---
class: nord-light

# Ex. 22 - Mechanics

https://editor.p5js.org/jcelerier/sketches/e82jlCy72

---
class: nord-light

# Ex. 23 - 3D Mechanics

https://editor.p5js.org/jcelerier/sketches/FIGFw5Ejc