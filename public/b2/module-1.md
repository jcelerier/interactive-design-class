layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 1: introduction

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light
background-image: url(static/substratePRN.jpg)

## What is this class about?

Writing programs which produce **beautiful art** out of math formulas.

![](static/substrate.jpg)

(_substrate_, j.tarbell, 2003, complexification.net)

---

class: center, middle, nord-light

# Let's explore the field

---

class: center, middle, nord-light

<blockquote class="twitter-tweet" align="center">
  <p lang="und" dir="ltr">
    <a href="https://t.co/REq9kneK4F">pic.twitter.com/REq9kneK4F</a></p>
    &mdash; Multi-Scale Truchet Patterns (@Truchet_Nested)
    <a href="https://twitter.com/Truchet_Nested/status/1320262517940158466?ref_src=twsrc%5Etfw">October 25, 2020</a>
</blockquote>

---

class: center, middle, nord-light

<blockquote class="twitter-tweet" align="left" width="500"><a href="https://t.co/Cf3KUHRfCv">pic.twitter.com/Cf3KUHRfCv</a> はぅ君 (@Hau_kun) <a href="https://twitter.com/Hau_kun/status/1320001165573058560?ref_src=twsrc%5Etfw">October 24, 2020</a></blockquote>


<blockquote class="twitter-tweet" align="right" width="500"><p lang="en" dir="ltr">Outline of a character. Source code -&gt; <a href="https://t.co/CEYCNdYGuH">https://t.co/CEYCNdYGuH</a> <a href="https://twitter.com/hashtag/openframeworks?src=hash&amp;ref_src=twsrc%5Etfw">#openframeworks</a> <a href="https://twitter.com/hashtag/programing?src=hash&amp;ref_src=twsrc%5Etfw">#programing</a> <a href="https://twitter.com/hashtag/creativecoding?src=hash&amp;ref_src=twsrc%5Etfw">#creativecoding</a> <a href="https://twitter.com/hashtag/%E3%83%97%E3%83%AD%E3%82%B0%E3%83%A9%E3%83%9F%E3%83%B3%E3%82%B0?src=hash&amp;ref_src=twsrc%5Etfw">#プログラミング</a> <a href="https://t.co/c3Xm5YjMAi">pic.twitter.com/c3Xm5YjMAi</a></p>&mdash; JunKiyoshi (@junkiyoshi) <a href="https://twitter.com/junkiyoshi/status/1320344528059072512?ref_src=twsrc%5Etfw">October 25, 2020</a></blockquote>


---

class: nord-light

## Twitter

[#newmediaart](https://twitter.com/hashtag/newmediaart) &nbsp;&nbsp;&nbsp;
[#creativecoding](https://twitter.com/hashtag/creativecoding) &nbsp;&nbsp;&nbsp;
[#proceduralart](https://twitter.com/hashtag/proceduralart)&nbsp;&nbsp;&nbsp;
[Julien_Espagnon](https://twitter.com/hashtag/Julien_Espagnon)&nbsp;&nbsp;&nbsp;
[MakeTechArt](https://twitter.com/hashtag/MakeTechArt)&nbsp;&nbsp;&nbsp;
[t_mao_p](https://twitter.com/hashtag/t_mao_p)

[#fractionisnoise](https://twitter.com/hashtag/fractionisnoise) &nbsp;&nbsp;&nbsp;
[#generative](https://twitter.com/hashtag/generative) &nbsp;&nbsp;&nbsp;
[algoritmic](https://twitter.com/algoritmic)&nbsp;&nbsp;&nbsp;
[memotv](https://twitter.com/memotv)&nbsp;&nbsp;&nbsp;
[v3ga](https://twitter.com/v3ga)&nbsp;&nbsp;&nbsp;
[retuneberlin](https://twitter.com/retuneberlin)

[#p5js](https://twitter.com/hashtag/p5js) &nbsp;&nbsp;&nbsp;
[#generativeart](https://twitter.com/hashtag/generativeart) &nbsp;&nbsp;&nbsp;
[#mediaarts](https://twitter.com/hashtag/mediaarts)&nbsp;&nbsp;&nbsp;
[codame](https://twitter.com/codame)&nbsp;&nbsp;&nbsp;
[nervous_jessica](https://twitter.com/nervous_jessica)&nbsp;&nbsp;&nbsp;
[CC_Amsterdam](https://twitter.com/CC_Amsterdam)

[#processing](https://twitter.com/hashtag/processing) &nbsp;&nbsp;&nbsp;
[#generativeartwork](https://twitter.com/hashtag/generativeartwork) &nbsp;&nbsp;&nbsp;
[supermedia_art](https://twitter.com/supermedia_art)&nbsp;&nbsp;&nbsp;
[t_ritoco](https://twitter.com/t_ritoco)&nbsp;&nbsp;&nbsp;
[nervous_system](https://twitter.com/nervous_system)&nbsp;&nbsp;&nbsp;
[halfdaft](https://twitter.com/halfdaft)

[#maxmsp](https://twitter.com/hashtag/maxmsp) &nbsp;&nbsp;&nbsp;
[#generativedesign](https://twitter.com/hashtag/generativedesign)&nbsp;&nbsp;&nbsp;
[dnmadenumerique](https://twitter.com/dnmadenumerique)&nbsp;&nbsp;&nbsp;
[okazz_](https://twitter.com/okazz_)&nbsp;&nbsp;&nbsp;
[Action_Script](https://twitter.com/Action_Script)&nbsp;&nbsp;&nbsp;
[REAS](https://twitter.com/REAS)

[#openframeworks](https://twitter.com/hashtag/openframeworks)&nbsp;&nbsp;&nbsp;
[#threejs](https://twitter.com/hashtag/threejs)&nbsp;&nbsp;&nbsp;
[generateme_blog](https://twitter.com/hashtag/generateme_blog)&nbsp;&nbsp;&nbsp;
[deconbatch](https://twitter.com/deconbatch)&nbsp;&nbsp;&nbsp;
[CookieDemoparty](https://twitter.com/CookieDemoparty)&nbsp;&nbsp;&nbsp;
[CreativeCodeLY](https://twitter.com/CreativeCodeLY)

[#openrndr](https://twitter.com/hashtag/openrndr) &nbsp;&nbsp;&nbsp;
[#cryptoart](https://twitter.com/hashtag/cryptoart) &nbsp;&nbsp;&nbsp;
[wblut](https://twitter.com/hashtag/wblut)&nbsp;&nbsp;&nbsp;
[dmitricherniak](https://twitter.com/dmitricherniak)&nbsp;&nbsp;&nbsp;
[Code4_11](https://twitter.com/Code4_11)&nbsp;&nbsp;&nbsp;
[sasj_nl](https://twitter.com/sasj_nl)

[#jsfiddle](https://twitter.com/hashtag/jsfiddle) &nbsp;&nbsp;&nbsp;
[#axidraw](https://twitter.com/hashtag/axidraw)&nbsp;&nbsp;&nbsp;
[onformative](https://twitter.com/hashtag/onformative)&nbsp;&nbsp;&nbsp;
[codeart_journal](https://twitter.com/codeart_journal)&nbsp;&nbsp;&nbsp;
[ky0ju_art](https://twitter.com/ky0ju_art)&nbsp;&nbsp;&nbsp;
[bitcraftlab](https://twitter.com/bitcraftlab)

[#shadertoy](https://twitter.com/hashtag/shadertoy)&nbsp;&nbsp;&nbsp;
[#procedural](https://twitter.com/hashtag/procedural)&nbsp;&nbsp;&nbsp;
[#glitch](https://twitter.com/hashtag/glitch)&nbsp;&nbsp;&nbsp;
[medialab](https://twitter.com/hashtag/medialab)&nbsp;&nbsp;&nbsp;
[nasana_x](https://twitter.com/nasana_x)


---

class: nord-light

## Some links to check

https://opinionatedguide.github.io/#/Design/d5-gen

https://github.com/jasonwebb/morphogenesis-resources

https://p5art.tumblr.com

https://js1024.fun/demos/2020

https://monoskop.org/Monoskop

https://www.openprocessing.org/browse/

https://generativeartistry.com/tutorials/circle-packing/

https://hamoid.com

---

class: nord-light

## Expos

https://pedalmarkt.com/blogs/news/ge-narrative-art-exhibition

https://www.katevassgalerie.com/blog/new-show-the-game-of-life-emergence-in-generative-art

---
class: nord-light

## IG, FB, etc.

Same hashtags, look for creative coding, generative...

[generativeartworks](https://www.instagram.com/generativeartworks/) &nbsp;&nbsp;&nbsp;
[datacode23](https://www.instagram.com/datacode23/)

[nonotakstudio](https://www.instagram.com/nonotakstudio/)

[joanielemercier](https://www.instagram.com/joanielemercier/)

[new_media_art](https://www.instagram.com/new_media_art/)

[zach.lieberman](https://www.instagram.com/zach.lieberman/)

[canvas.51](https://www.instagram.com/canvas.51)

[creativecodeart](https://www.instagram.com/creativecodeart/)

[generative.hut](https://www.instagram.com/generative.hut/)

---
class: nord-light

# Useful books
Generative Design: https://www.amazon.fr/Generative-Design-Visualize-Program-Javascript/dp/1616897589

Form+Code: https://www.amazon.fr/Form-Code-Design-Art-Architecture/dp/1568989377

Generative art: https://www.amazon.fr/Generative-Art-Matt-Pearson/dp/1935182625

Data Visualisation: https://www.amazon.fr/Data-Visualisation-Handbook-Driven-Design/dp/1526468921/

Nature of Code: https://natureofcode.com/

# Useful youtube channels
https://www.youtube.com/playlist?list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA

https://www.youtube.com/c/TheCodingTrain/videos

---

class: center, middle, nord-dark

### w̷̧̩̾̎r̶̙̮̓ì̵̳̪t̶̖͒̏i̶̙̗͊͋n̴̞̮̂͒g̷̠̒ p̶̹͂r̴̯͖̅o̴̧̩̓̕g̵͕̻̓̌r̵̼̲̆̇a̵̡͌̐m̶̧͚͒̌s̵̨͔̽̿

---

class: center, middle, nord-dark

# M̴̧̛͙̘̝̠̤̹̋̑̅̐͆̒͗̾̔̓́̈͐̉͘͘̚̕͠͝͝͠ ̸̧̧̡̢͎̣̜̦̦̲̼̭͍̯̥͙̯̲̲̭͇͇̯̞͋̑͆̄͐̈̇̌͂͐̇̾̀́̓̈́́̈́͊̚͘͝͝͝ͅA̴̡̨̛̺̹͖̥̒̾̉̈́͗͂̑̓͌͌̎̕͜͠ ̵̢̯͙͔̌̏̽̇́͋̒͒̀̈́̐̃̽̒̌̌͝T̸̢͕̰̹͔̝̟̙͚̦̠̩̗̙̠̣̙͍͍̼͚͂̒̐̍̈͂̂͛̓̐̾̚͘͠͠ ̵̘͎͙̜͎̼̠͈͚̱͔̪̖̭̻̞͇̓̊̾̾̆͗͒͊͂͆Ḣ̶̛͎̫͈̝̗͇̬̣̖͉̼̫̗̣̙̟̯̞̬̭̜͚͔̥͗̀̃̎̐̀̍̈́̓̂͐̍̉͋̈́͛͊̔͌̒̊̀̀̀̕͜͝͠͠͝͝͝ ̶̫̃Ş̸̪̯̳̝͈̥͕͍͓̖͓͉̮͔̣̼͔̲̪̟̳̖̣͓̺̯̰͎̅̈́͗̐̾͊͆̄̇͗̓̏̿͌̑͂̔̓̈́̿̔̉̈́͗͆͑̿̍͊̎̉͘͜͝͠

---

class: nord-light

## Processing drawing example

```js
const W = 200;
function setup() {
  createCanvas(W*2, W*2);
  colorMode(HSB,99);
  clear();
  background(0);
}

function draw() {
 stroke(frameCount % 99, 99, 99);
 translate(W,W);
 for(let i = 0; i < 12; i++)
 {
  rotate(PI/6*(-1)**i);
  scale(1,-1);
  if(mouseIsPressed)
    line(mouseX-W, mouseY-W, pmouseX-W, pmouseY-W);
 }
}
```

(source: https://twitter.com/ryotakob/status/1320350933394415616)

---

class: nord-light

## Processing animation example

```js
function setup() {
  createCanvas(115, 115);
}

function draw()  {
  background(20);
  const circleWidth = 50;
  const circleHeight = 50;
  for(var i = 0; i < 10; i++)
  {
    for(var j = 0; j < 10; j++)
    {
      const col = (i * j + 50 + frameCount) % 255;
      fill(col);
      stroke(col);

      const x = i / 10;
      const y = j / 10;
      ellipse(x * 100, y * 100,
       circleWidth,
       circleHeight);
    }
  }
}
```

---

class: nord-light

# How it's gonna work

#### - Learn math & programming at the same time

#### - Practice ! Daily sketches !

#### - At the end of the class you will be able to recreate a lot of examples

#### - My goal: transmit you the basic tools & techniques for you to make your own art
##### (And client work !)
---

class: nord-light

<span>&nbsp;&nbsp;&nbsp;&nbsp;</span>**Module 1**
- Introduction, basics of programming with p5.js, basic maths (4h)
- Guided project (4h)<br/>
**Module 2**
- Introduction to motion design, interactivity, OOP & advanced programming (4h)
- Guided project (4h)<br/>
**Module 3**
- Advanced generative design techniques, morphogenesis, grammars, generative typography (4h)
- Guided project (4h)<br/>
**Module 4**
- Exploration of other techs, data visualization, web integration (4h)
- Guided project (4h)<br/>
**Module 5**
- Group workshop (8h)

---

class: nord-light

# Programming

It's about telling the computer what to do.

The computer follows orders, very strictly.

Our means of communication with the computer is _text_.

This _text_ is written in a specific language, a _programming language_.

There are *many* *many* *many* programming languages.

We will use one called Javascript, with a library called _p5.js_

&nbsp; &nbsp; &nbsp; ⚠ The concepts will apply to most other programming languages !

---

class: nord-light

# Maths

Programming itself is _entirely_ maths.

Making pretty things often leverages a lot of basic mathematic tools:
* Trigonometry (sin, cos) & usual functions (exponential, absolute value...)
* Derivatives
* Probability laws
* Linear interpolation
* Matrices
* Geometry

The programming language syntax makes using those easy - but we must not forget that they are always here... lurking...

---

class: nord-dark, center, middle

# Let's get started !

### Open https://editor.p5js.org

---

class: nord-light

## Drawing a line

```js
function setup() {
  line(0, 50, 100, 50);
}
```

---

class: nord-light

## Drawing a circle

```js
function setup() {
  ellipse(50, 50, 10, 10);
}
```

---

class: nord-light

## Drawing a circle... on a black background

```js
function setup() {
  background(0, 0, 0);
  ellipse(50, 50, 30, 30);
}
```

---

class: nord-light

## Funkier circle

```js
function setup() {
  background(0, 0, 0);
  stroke(255, 0, 255);
  fill(0, 255, 255);
  strokeWeight(5);
  ellipse(50, 50, 30, 30);
}
```

---

class: nord-light

## Setting a pixel

```js
function setup() {
  set(10, 10, color(255, 0, 0));
  updatePixels();
}
```

---

class: nord-dark, center, middle

# What if you don't remember the language ?

### ⟶ https://p5js.org/reference

---

class: nord-light

# Important things to ponder

##### What is the vocabulary of the sentences we are using to communicate with the computer ?

##### What is their syntax ?

##### What is their meaning (semantics) ?

---

class: nord-dark

# Free composition !

You've got 10 minutes :-)

---

class: nord-dark

# Let's look at how to improve things

---

class: nord-light

## Drawing a lot of lines

```js
function setup() {
  // Horizontal
  line(0,  0, 100, 0);
  line(0, 10, 100, 10);
  line(0, 20, 100, 20);
  line(0, 30, 100, 30);
  line(0, 40, 100, 40);
  line(0, 50, 100, 50);

  // Vertical (oops, bug !)
  line(0,  0,  0, 100);
  line(10, 0, 10, 100);
  line(20, 0, 10, 100);
  line(30, 0, 20, 100);
  line(40, 0, 30, 100);
  line(50, 0, 40, 100);
}
```

---

class: nord-light

## Drawing a lot of lines

```js
function setup() {
  let size = 100;
  // Horizontal
  line(0,  0, size, 0);
  line(0, 10, size, 10);
  line(0, 20, size, 20);
  line(0, 30, size, 30);
  line(0, 40, size, 40);
  line(0, 50, size, 50);

  // Vertical
  line(0,  0,  0, size);
  line(10, 0, 10, size);
  line(20, 0, 20, size);
  line(30, 0, 30, size);
  line(40, 0, 40, size);
  line(50, 0, 50, size);
}
```

---

class: nord-light

## Drawing a lot of lines

```js
function setup() {
  let size = 100;
  let separation = 10;

  line(0, 0 * separation, size, 0 * separation);
  line(0, 1 * separation, size, 1 * separation);
  line(0, 2 * separation, size, 2 * separation);
  line(0, 3 * separation, size, 3 * separation);
  line(0, 4 * separation, size, 4 * separation);
  line(0, 5 * separation, size, 5 * separation);
}
```

---

class: nord-light

## Drawing a lot of lines

```js
function setup() {
  // Horizontal
  for(let i = 0; i < 50; i += 10) {
    line(0,  i, 100, i);
  }

  // Vertical (yay, no bug)
  for(let i = 0; i < 50; i += 10) {
    line(i, 0, i, 100);
  }
}
```

---

class: nord-light

## Drawing a lot of lines

```js
function setup() {
  // Both combined: less code, less opportunity for bugs
  // Best code is the one you don't write !
  for(let i = 0; i < 50; i += 10) {
    line(0, i, 100, i);
    line(i, 0,   i, 100);
  }
}
```

---

class: nord-light

## What if we want the even lines to be in red ?

```js
function setup() {
  // Horizontal
  for(let i = 0; i < 50; i += 10) {

    // ?

    line(0, i, 100, i);
    line(i, 0,   i, 100);
  }
}
```

---

class: nord-light

## What if we want the even lines to be in red ?

```js
function setup() {
  // Horizontal
  for(let i = 0; i < 50; i += 10) {

    /* if i is even */ stroke(255, 0, 0);
    /* else if i is odd */ stroke(0, 0, 0);

    line(0, i, 100, i);
    line(i, 0,   i, 100);
  }
}
```

---

class: nord-light

## What if we want the even lines to be in red ?

```js
function setup() {
  // Horizontal
  for(let i = 0; i < 50; i += 10) {

    if((i / 10) % 2 == 0) {
      stroke(255, 0, 0);
    } else {
      stroke(0, 0, 0);
    }

    line(0, i, 100, i);
    // ?
    line(i, 0,   i, 100);
  }
}
```

---

class: nord-light

## Computational color theory

RGB, HSV, ... let's check Wikipedia !

https://fr.wikipedia.org/wiki/CIE_RGB

https://fr.wikipedia.org/wiki/Teinte_Saturation_Valeur

---

class: nord-dark

# Free composition !

You've got 10 minutes :-)

---

class: nord-dark, center, middle

# Exercice: let's make a triangle

---

class: nord-dark, center, middle

# Exercice: let's make a square


---

class: nord-dark, center, middle

# Exercice: let's make a pentagon

---

class: nord-light

# Super super important formulas

Trigonometry is used all over the place in generative design.

Go from polar coordinates to cartesian coordinates.
<p>$$\Large x = r * cos(\theta)~~~~~y = r * sin(\theta)$$</p>

Go from cartesian coordinates to polar coordinates.
<p>$$\Large r = sqrt(x*x + y*y)~~~~~\theta = atan2(y, x)$$</p>

(things are a bit more complicated but we'll skip that for now :-))

See https://editor.p5js.org/jcelerier/sketches/acPp467og for an animation.

---
class: nord-dark

# Parametric equations

Parametric equations: what we've been doing when setting `x=sin(...), y=cos(...)`.

General idea: define a curve with
```js
let x = a_function_of(t);
let y = another_function_of(t);
// 3D : let z = yet_another_function_of(t);
```

---

class: nord-dark, middle

# Function syntax
```js
function myFunction(var a, var b) {
  line(a + b, a - b, 100, 100);
}
...

myFunction(10, 50);
```

---

class: nord-dark, middle

# Cheat sheet
```js
// Comment
/* Also comment */

// Variable declaration
let myVariable = /* some value */;

// Assignment
myVariable = /* some other value */;

// Conditions
if(myCondition) {

} else if(myOtherConditon) {

} else {

}
```

---

class: nord-dark, middle

# Cheat sheet
```js
// Loops
for(let myCounter = 0; myCounter < max; myCounter++) {
  // this is repeated "max" times
}

for(/* counter initialization*/; /* loop condition */; /* post-loop operation */) {
}

```

---

class: nord-light

# Advanced usage

- Install VSCode (https://code.visualstudio.com/)
- Install p5.vscode extension
- Create a new project and check that everything works