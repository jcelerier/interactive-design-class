layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **generative & interactive design**
### module 5: group project

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>


---
class: nord-light

# Step 1: project definition / selection
# Step 2: case study
# Step 3: realisation
# 20 minute presentation at the end of the semester



---
class: nord-light

# Project 1: fish pond

* https://www.youtube.com/watch?v=KLTMGFWpkro
* https://www.thisiscolossal.com/2016/08/interactive-koi-teamlab/
* https://www.youtube.com/watch?v=969eAMj05JY

---
class: nord-light

# Project 2: interactive map

* A websites that shows countries individually - the user can choose which country is displayed.
* Countries morph into each other.
* Must look fancy.
* Ideally works on mobile.

---
class: nord-light

# Project 3: particle sketch

* Given a picture, use particles to sketch its main features / outline / ... in an animated way.

* Possible inspiration: https://www.youtube.com/watch?v=JkcmwCotVew


---
class: nord-light

# Project 4: procedural tattoos

* Generate tattoo patterns procedurally.
* Display them on a 3d model that can be rotated.

* Possible inspiration:

  https://www.superhitideas.com/amazing-abstract-tattoo-designs-collection/


---
class: nord-light

# Project 5: Garden of the Earthly Delights

* Provide your own interpretation of the famous Hieronymus Bosch painting.
* Has to be animated in a meaningful way.

![the painting](https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/The_Garden_of_earthly_delights.jpg/1280px-The_Garden_of_earthly_delights.jpg)

---
class: nord-light

# Project 6: a window on the world

* The material: https://www.insecam.org/en/byrating/
* Possible inspirations:
 - https://rndr.studio/projects/hyperlocator/
 - https://www.stanza.co.uk/
 - https://www.theartstory.org/movement/video-art/
 - https://www.pinterest.fr/pin/449374869047291462/

---
class: nord-light

# Project 7: old-school fun

* Something that reminds old video games and computers
* Inspiration: https://vimeo.com/462343366
* Inspiration: https://pouet.net
  * For instance, making a few dozen seconds of animations and transitions that look like the ones here: https://www.amigafrance.com/revision-2020-le-best-of/
  or here: https://www.youtube.com/watch?v=zwoyfH7TgEQ



