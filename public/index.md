layout: true
class: typo, typo-selection
site-title: Generative Design - Cours - B1

---

count: false
class: left, middle, nord-light

# **generative & interactive design**

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light
<br/><br/>

Module 1: [Courses](b1/module-1.html) / [Exercises](b1/module-1.ex.html) / [Correction](b1/module-1.corr.html)

Module 2: [Courses](b1/module-2.html) / [Exercises](b1/module-2.ex.html)

Module 3: [Courses](b1/module-3.html) / [Exercises](b1/module-3.ex.html)

Projet: [Énoncé](b1/module-4.html)

------

[Reference](b1/reference.html) / [Practice](b1/practice.html)
