#!/bin/zsh

for file in b2/*.md; do
  TITLE=$(grep site-title $file | cut -d ':' -f2 | awk '{ gsub(/^[ \t]+|[ \t]+$/, ""); print }')
  MARKDOWN=$(basename $file)
  BASENAME=$(echo $MARKDOWN | cut -d. -f1)
  DIRNAME=$(dirname $file)
  sed "s/@TITLE@/$TITLE/;s/@MARKDOWN@/$MARKDOWN/" js/template.html > $DIRNAME/$BASENAME.html
done
